﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using InputControl;
using UnityEngine;

namespace Preferences.Serialization
{
	public static class GamePreferencesSerializer {
		
		// TODO: implement
		public static void SerializeSettings(VideoSettings settings)
		{
			
		}
		
		// TODO: implement
		public static void SerializeSettings(AudioSettings settings)
		{
			
		}
		
		// TODO: implement
		public static void SerializeSettings(GameSettings settings)
		{
			
		}

		public static void SerializeSession(Session session)
		{
			var fileStream = File.Open(Application.persistentDataPath + "/session.dat", FileMode.OpenOrCreate);
			var binaryFormatter = new BinaryFormatter();
			binaryFormatter.Serialize(fileStream, session);
			fileStream.Close();
		}

		public static bool DeserializeSession(ref Session session)
		{
			if (!File.Exists(Application.persistentDataPath + "/session.dat")) return false;
			var fileStream = File.Open(Application.persistentDataPath + "/session.dat", FileMode.Open);
			var binaryFormatter = new BinaryFormatter();
			session = (Session) binaryFormatter.Deserialize(fileStream);
			fileStream.Close();
			return true;
		}
	}
}
