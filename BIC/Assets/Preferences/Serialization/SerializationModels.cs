﻿using System;
using InputControl;
using UnityEngine;

namespace Preferences.Serialization
{
	[Serializable]
	public class VideoSettings
	{
		public static Resolution Resolution;
		public static bool FullScreen;
		public static int VSync;
	}
	
	[Serializable]
	public class AudioSettings
	{
		public static float MusicVolume;
		public static float EffectsVolume;
	}

	[Serializable]
	public class GameSettings
	{
		public static bool Blood;
		public static float BloodAmount;
		// TODO: create enum for Difficulty
		public static int Difficulty;
	}
	
	[Serializable]
	public class Session
	{
		public string[] CurrentSkins = { "Default", "Default", "Default" };
		public string LastLevel = "level_demo";
		public string[] PlayerInputDevices { get; } = {null, null, null};
	}
}
