﻿using Configs.Player;
using InputControl;
using Preferences.Serialization;

namespace Preferences
{
	public static class Prefs
	{
	    public static int Tokens = 0;
		public static InputDevice[] PlayerInputDevices { get; } = {null, null, null};
		public static PlayerKeyboardControls[] PlayerKeyboardControls { get; } = {null, null, null};
		
		private static VideoSettings _videoSettings;
		private static AudioSettings _audioSettings;
		private static GameSettings _gameSettings;
		private static Session _session;

		public static Session Session
		{
			get
			{
				if (_session != null || GamePreferencesSerializer.DeserializeSession(ref _session)) return _session;
				_session = new Session();
				SavePreferences();
				
				return _session;
			}
		}
		
		// TODO: add Getters

		public static void SavePreferences()
		{
			GamePreferencesSerializer.SerializeSession(_session);
		}
	}
}
