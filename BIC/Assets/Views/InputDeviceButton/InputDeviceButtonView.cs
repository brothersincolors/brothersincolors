﻿using System;
using DataBases.KeycodesDataBase;
using UnityEngine;
using UnityEngine.UI;

namespace Views.InputDeviceButton
{
	public class InputDeviceButtonView : MonoBehaviour
	{
		public Image Image;
		public string GamepadKeyName;
		public string KeyboardKeyName;

		public void SetGamepadSprite(string modelName) =>
			Image.sprite = KeycodeDataBase.Instance.GetUIModelByName(modelName + ' ' + GamepadKeyName).ButtonSprite;
		
		public void SetKeyboardSprite() =>
			Image.sprite = KeycodeDataBase.Instance.GetUIModelByName("KEYBOARD " + KeyboardKeyName).ButtonSprite;

	}
}
