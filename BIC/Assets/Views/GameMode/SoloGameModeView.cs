﻿using Controllers.GameControllers;
using UnityEngine;
using UnityEngine.UI;

namespace Views.GameMode
{
	public class SoloGameModeView : MonoBehaviour
	{

		public SoloModeController SoloModeController;
		
		public Image CurrentPlayer;
		public Image LeftPlayer;
		public Image RightPlayer;

		private bool _isActivated;

		public void ActivateTips()
		{
			_isActivated = true;
			CurrentPlayer.gameObject.SetActive(true);
			LeftPlayer.gameObject.SetActive(true);
			RightPlayer.gameObject.SetActive(true);
		}
		
		public void DeactivateTips()
		{
			_isActivated = false;
			CurrentPlayer.gameObject.SetActive(false);
			LeftPlayer.gameObject.SetActive(false);
			RightPlayer.gameObject.SetActive(false);
		}

		public void RecolorTips()
		{
			CurrentPlayer.color = SoloModeController.PlayerControllers[SoloModeController.ControlledPlayerIndex].Color;
			LeftPlayer.color = SoloModeController.PlayerControllers[
				SoloModeController.FixPlayerIndex(SoloModeController.ControlledPlayerIndex - 1)
			].Color;
			RightPlayer.color = SoloModeController.PlayerControllers[
				SoloModeController.FixPlayerIndex(SoloModeController.ControlledPlayerIndex + 1)
			].Color;
		}
		
		private void Awake()
		{
			DeactivateTips();
		}
	}
}
