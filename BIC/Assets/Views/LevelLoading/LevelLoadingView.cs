﻿using Controllers.LevelManagement;
using UnityEngine;

namespace Views.LevelLoading
{
    public class LevelLoadingView : MonoBehaviour
    {
        public static LevelLoadingView Instance;

        private SceneManagmentController _sceneManagmentController;

        private void Awake() => Instance = this;

        public void Start()
        {
            _sceneManagmentController = SceneManagmentController.Instance;
        }

        public async void LoadSceneByName(string sceneName)
        {
            await _sceneManagmentController.LoadSceneByName(sceneName);
        }
    }
}