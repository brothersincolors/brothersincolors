﻿using System.Collections;
using System.Security.Permissions;
using Configs.Player;
using Controllers.UI.Lobby;
using Controllers.UI.Lobby.Customization;
using DataBases.KeycodesDataBase;
using DataBases.SkinDataBase;
using Events;
using Extensions;
using InputControl;
using UnityEngine;
using UnityEngine.UI;
using Views.InputDeviceButton;
using Button = UserInterface.Scripts.Button;
using SkinQuality = Configs.Skins.SkinQuality;
using Toggle = UserInterface.Scripts.Toggle;

namespace Views.Lobby.Customization
{
	public class CustomizationView : MonoBehaviour
	{

		public CustomizationController CustomizationController;
		public GameObject Insides;

		public Button NextSkinButton;
		public Button PreviousSkinButton;

		public Animator PlayerAnimator;
		public Toggle ReadyToggle;
		public GameObject BackgroundTip;

		public InputDeviceButtonView[] InputDeviceButtonViews;
		
		public void Activate()
		{
			Insides.SetActive(true);
			BackgroundTip.SetActive(false);
			UpdateButtonsImages();
		}

		public void Deactivate()
		{
			Insides.SetActive(false);
			BackgroundTip.SetActive(true);
		}

		private void DisplaySkin(SkinModel playerSkin) => PlayerAnimator.runtimeAnimatorController = playerSkin.RuntimeAnimatorController;

		private void UpdateButtonsImages()
		{
			InputDeviceButtonViews.ForEach(button =>
			{
				if (CustomizationController.InputDevice != null)
					button.SetGamepadSprite(CustomizationController.InputDevice.GetDeviceName().Contains("Sony")
						? "PS3"
						: "XBOX");
				else
					button.SetKeyboardSprite();
			});
		}
		
		private void Start()
		{	
			PreviousSkinButton.OnClick.AddListener(() =>
			{
				DisplaySkin(CustomizationController.GetPreviousSkin());
			});
			
			NextSkinButton.OnClick.AddListener(() =>
			{
				DisplaySkin(CustomizationController.GetNextSkin());
			});
			DisplaySkin(SkinDataBase.Instance.GetPlayerSkin(Preferences.Prefs.Session.CurrentSkins[(int) CustomizationController.PlayerColor]));
			
			ReadyToggle.OnClick.AddListener(state =>
			{
				if (state)
				{
					switch (GameStateMachine.CurrentGameMode)
					{
						case GameStateMachine.GameMode.Solo:
							SoloLobbyController.Instance.ActivatedToggles++;
							break;
						case GameStateMachine.GameMode.Tripple:
							TrippleLobbyController.Instance.ActivatedToggles++;
							break;

					}
				}
				else
				{
					switch (GameStateMachine.CurrentGameMode)
					{
						case GameStateMachine.GameMode.Solo:
							SoloLobbyController.Instance.ActivatedToggles--;
							break;
						case GameStateMachine.GameMode.Tripple:
							TrippleLobbyController.Instance.ActivatedToggles--;
							break;

					}
				}
			});
			Insides.SetActive(false);
			BackgroundTip.SetActive(true);
		}

		private void Update()
		{
			if (!CustomizationController.Activated) return;
			
			if (CustomizationController.InputDevice != null)
			{
				if (CustomizationController.InputDevice.GetKeyDown(GamepadKeycode.L2))
					PreviousSkinButton.Click();
				if (CustomizationController.InputDevice.GetKeyDown(GamepadKeycode.R2))
					NextSkinButton.Click();
				if (CustomizationController.InputDevice.GetKeyDown(GamepadKeycode.Action3))
					ReadyToggle.Click();
			}
			else
			{
				if (Input.GetKeyDown(CustomizationController.KeyboardControls.Left))
					PreviousSkinButton.Click();
				if (Input.GetKeyDown(CustomizationController.KeyboardControls.Right))
					NextSkinButton.Click();
				if (Input.GetKeyDown(CustomizationController.KeyboardControls.Jump))
					ReadyToggle.Click();
			}
		}
	}
}
