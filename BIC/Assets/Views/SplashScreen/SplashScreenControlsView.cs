﻿using UnityEngine;

namespace Views.SplashScreen
{
	public class SplashScreenControlsView : MonoBehaviour
	{
		public int PlayerIndex;

		public GameObject ControlsHolder;
		
		private void Start()
		{
			if (Preferences.Prefs.PlayerInputDevices[PlayerIndex] != null)
			{
				if (Preferences.Prefs.PlayerInputDevices[PlayerIndex].GetDeviceName().Contains("PS"))
					ActivateButtons(1);
				else
					ActivateButtons(0);
			}
			else
			{
				ActivateButtons(2);
			}
			
				
		}

		private void ActivateButtons(int buttonIndex)
		{
			foreach (Transform controls in ControlsHolder.transform)
			{
				controls.GetChild(0).GetChild(buttonIndex).gameObject.SetActive(true);
			}
		}
	}
}
