﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crate : MonoBehaviour {

    private ParticleSystem system;

    private void Start()
    {
        system = GetComponentInChildren<ParticleSystem>();
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.CompareTag("Player"))
        {
            system.Emit(30);
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<Collider2D>().enabled = false;
            Invoke("DestroySelf", 4f);
        }
    }

    private void DestroySelf()
    {
        Destroy(gameObject);
    }
}
