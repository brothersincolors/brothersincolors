﻿using Events;
using ShaksUI;
using UnityEngine;
using UnityEngine.Events;

namespace UserInterface.Scripts
{
	public class Toggle : MonoBehaviour
	{

		public UnityEventBool OnClick;

		public GameObject ActivatedObject;
		public GameObject DeactivetedObject;

		public bool IsOn => _isOn;
		public bool Interactable;

		private bool _isOn;
		

		public void Click()
		{
			if (GameStateMachine.CurrentGameState == GameStateMachine.GameState.Loading) return;
			if (!Interactable) return;
			_isOn = !_isOn;
			ActivatedObject.SetActive(_isOn);
			DeactivetedObject.SetActive(!_isOn);
			OnClick.Invoke(_isOn);
		}

		public void Select()
		{
			
		}

		public void Deselect()
		{
			
		}

		private void Start()
		{
			ActivatedObject.SetActive(false);
			DeactivetedObject.SetActive(true);
		}
	}
}
