﻿using System.Collections;
using System.Collections.Generic;
using DataBases.KeycodesDataBase;
using UnityEngine;
using UnityEngine.UI;
using InputControl;

public class ScrollbarViaGamepad : MonoBehaviour {

    /* Прокручивание скроллбара при помощи геймада
     * Должен висеть на скроллбаре
    */

    private Scrollbar attScrollbar;

    private void Start()
    {
        attScrollbar = GetComponent<Scrollbar>();
    }

    private void Update()
    {
        attScrollbar.value += InputManager.GlobalJoystick.GetAxis(GamepadKeycode.RightStickY);
    }
}
