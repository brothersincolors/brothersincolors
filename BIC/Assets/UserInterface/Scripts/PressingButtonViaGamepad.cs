﻿using DataBases.KeycodesDataBase;
using UnityEngine;
using UnityEngine.EventSystems;
using InputControl;
using UnityEngine.UI;

public class PressingButtonViaGamepad : MonoBehaviour {

    /* Позволяет нажимать на кнопки при помощи геймпадов разных типов, так как
     * оси разные для каждого геймпада. Скрипт должен висеть на Event System
     */

    private EventSystem attEventSystem;

    private Dropdown selectedDropdown;  // При открытии какого-то Dropdown запоминает что он открыт

    public void SetSelectedButton(Button button)  // Метод необходим при смене Fragment, чтобы заселектить кнопку
    {
        attEventSystem.SetSelectedGameObject(button.gameObject);
        button.OnSelect(new BaseEventData(attEventSystem));
    }

    private void Start()
    {
        attEventSystem = GetComponent<EventSystem>();
    }


    private void Update()
    {
        // Если нажали на кнопку геймпада или enter
        if (InputManager.GlobalJoystick.GetKeyDown(GamepadKeycode.Action3) || Input.GetKeyDown(KeyCode.Return))
        {
            // Если выделена какая-нибудь кнопка
            if (attEventSystem.currentSelectedGameObject != null)
            {
                // Если это кнопка, то вызываем ее event'ы
                var button = attEventSystem.currentSelectedGameObject.GetComponent<Button>();
                if (button != null)
                {
                    attEventSystem.SetSelectedGameObject(null);
                    button.onClick.Invoke();
                    button.OnDeselect(new BaseEventData(EventSystem.current));
                    return;
                }
                // Если это dropdown, то открываем его.
                var dropdown = attEventSystem.currentSelectedGameObject.GetComponent<Dropdown>();
                if (dropdown)
                {
                    selectedDropdown = dropdown;
                    dropdown.Show();
                    return;
                }
                // Если это toggle, то активируем его
                var toggle = attEventSystem.currentSelectedGameObject.GetComponent<Toggle>();
                if (toggle)
                {
                    toggle.isOn = !toggle.isOn;
                    toggle.onValueChanged.Invoke(toggle.isOn);
                    // А если он находится в Dropdown, закрываем dropdown и выставляем dropdown значение
                    // этого toggle
                    if (selectedDropdown)
                    {
                        int number = int.Parse(toggle.name.Split(' ')[1].Replace(':', '0')) / 10;
                        selectedDropdown.value = number;
                        selectedDropdown = null;
                    }
                }
            }
        }
    }
}
