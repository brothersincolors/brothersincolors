﻿using Events;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UserInterface.Scripts
{
	public class Button : MonoBehaviour
	{

		public UnityEvent OnClick;

		private bool _selected;

		public void Click()
		{
			if (GameStateMachine.CurrentGameState == GameStateMachine.GameState.Loading) return;
			OnClick.Invoke();
		}

		public void Select()
		{
			_selected = true;
		}

		public void Deselect()
		{
			_selected = false;
		}
	}
}
