﻿using System;
using UnityEngine;

namespace Configs.Player
{
    [Serializable]
    public struct PlayerColor
    {
        public string Name;
        public Color Color;
    }
}