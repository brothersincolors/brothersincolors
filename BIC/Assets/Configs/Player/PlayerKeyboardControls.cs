﻿using System;
using UnityEngine;

namespace Configs.Player
{
    [Serializable]
    public class PlayerKeyboardControls
    {
        [SerializeField]
        private readonly int _playerIndex;

        public PlayerKeyboardControls(int playerIndex)
        {
            _playerIndex = playerIndex;
        }

        public KeyCode Right
        {
            get
            {
                switch (_playerIndex)
                {
                    case 0: return KeyCode.D;
                    case 1: return KeyCode.L;
                    default: return KeyCode.RightArrow;
                }
            }
        }

        public KeyCode Left
        {
            get
            {
                switch (_playerIndex)
                {
                    case 0: return KeyCode.A;
                    case 1: return KeyCode.J;
                    case 2: return KeyCode.LeftArrow;
                }

                return KeyCode.None;
            }
        }

        public KeyCode Jump
        {
            get
            {
                switch (_playerIndex)
                {
                    case 0: return KeyCode.W;
                    case 1: return KeyCode.I;
                    case 2: return KeyCode.UpArrow;
                }

                return KeyCode.None;
            }
        }

        public KeyCode Roll
        {
            get
            {
                switch (_playerIndex)
                {
                    case 0: return KeyCode.Alpha1;
                    case 1: return KeyCode.Alpha2;
                    case 2: return KeyCode.Alpha3;
                }

                return KeyCode.None;
            }
        }

        public KeyCode Interact
        {
            get
            {
                switch (_playerIndex)
                {
                    case 0: return KeyCode.S;
                    case 1: return KeyCode.K;
                    case 2: return KeyCode.DownArrow;
                }

                return KeyCode.None;
            }
        }

        public KeyCode Punch
        {
            get
            {
                switch (_playerIndex)
                {
                    case 0: return KeyCode.S;
                    case 1: return KeyCode.K;
                    case 2: return KeyCode.DownArrow;
                }

                return KeyCode.None;
            }
        }
    }
}