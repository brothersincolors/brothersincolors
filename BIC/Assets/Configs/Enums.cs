﻿namespace Configs
{
    public enum DefaultPlayerColor
    {
        Red = 0,
        Green = 1, 
        Blue = 2
    }

    public enum Difficulty
    {
        Easy,
        Normal,
        Hard
    }

    namespace Skins
    {
        public enum SkinQuality
        {
            Common,
            Uncommon,
            Rare
        }

        public enum SkinColor
        {
            Red = 0,
            Green = 1,
            Blue = 2,
            None = -1
        }
        
    }
}
