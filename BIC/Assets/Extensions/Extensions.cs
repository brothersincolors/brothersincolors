﻿using System;
using UnityEngine;
using Views.InputDeviceButton;

namespace Extensions
{
    public static class Extensions {

        public static int ToInt(this bool self)
        {
            return self ? 1 : 0;
        }
        
        public static Color Summ(this Color self, Color b)
        {
            var result = new Color(self.r + b.r, self.g + b.g, self.b + b.b);
            if (result.r > 1) result.r = 1;
            if (result.g > 1) result.g = 1;
            if (result.b > 1) result.b = 1;
            result.a = 1;
            return result;
        }
        
        public static void ForEach(this InputDeviceButtonView[] self, Action<InputDeviceButtonView> action)
        {
            foreach (var button in self)
                action(button);
        }
    }
}
