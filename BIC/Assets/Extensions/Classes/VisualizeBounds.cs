﻿using UnityEngine;

namespace Extensions.Classes
{
	public class VisualizeBounds : MonoBehaviour {

		SpriteRenderer attRenderer;

		void Start () 
		{
			attRenderer = GetComponent<SpriteRenderer>();
		}
	
		void Update () 
		{
			Debug.DrawLine(new Vector3(attRenderer.sprite.bounds.min.x, attRenderer.sprite.bounds.max.y) + transform.position, new Vector3(attRenderer.sprite.bounds.max.x, attRenderer.sprite.bounds.max.y) + transform.position, Color.red);
			Debug.DrawLine(new Vector3(attRenderer.sprite.bounds.min.x, attRenderer.sprite.bounds.min.y) + transform.position, new Vector3(attRenderer.sprite.bounds.max.x, attRenderer.sprite.bounds.min.y) + transform.position, Color.red);
			Debug.DrawLine(new Vector3(attRenderer.sprite.bounds.min.x, attRenderer.sprite.bounds.min.y) + transform.position, new Vector3(attRenderer.sprite.bounds.min.x, attRenderer.sprite.bounds.max.y) + transform.position, Color.red);
			Debug.DrawLine(new Vector3(attRenderer.sprite.bounds.max.x, attRenderer.sprite.bounds.min.y) + transform.position, new Vector3(attRenderer.sprite.bounds.max.x, attRenderer.sprite.bounds.max.y) + transform.position, Color.red);
		}
	}
}
