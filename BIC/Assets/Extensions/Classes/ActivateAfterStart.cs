﻿using UnityEngine;

namespace Extensions.Classes
{
	public class ActivateAfterStart : MonoBehaviour {

		void Start () {
			if (!gameObject.activeSelf) gameObject.SetActive(true);
		}
	}
}
