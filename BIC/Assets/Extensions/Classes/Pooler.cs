﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Extensions.Classes
{
    public class Pooler : MonoBehaviour {

        private int _count;
        private readonly List<GameObject> _poolObjects = new List<GameObject>();

        public GameObject ActivateObject()
        {
            if (_poolObjects == null || !_poolObjects.Any()) 
            {
                Debug.LogError("Pooler is not initialized.");
                return null; 
            }
            var obj = _poolObjects.First(ob => { return !ob.activeSelf; });
            obj.SetActive(true);
            return obj;
        }

        public GameObject ActivateObject(Vector3 position, Quaternion rotation)
        {
            if (_poolObjects == null || _poolObjects.Count() == 0)
            {
                Debug.LogError("Pooler is not initialized.");
                return null;
            }
            var obj = _poolObjects.First((GameObject ob) => { return !ob.activeSelf; });
            obj.transform.position = position;
            obj.transform.rotation = rotation;
            obj.SetActive(true);
            return obj;
        }

        public List<GameObject> Initialize(GameObject prefab, int count)
        {
            this._count += count;
            List<GameObject> list = new List<GameObject>();
            for (int i = 0; i < count; i++)
            {
                _poolObjects.Add(Instantiate(prefab, transform.position,Quaternion.identity, transform));
                _poolObjects.Last().SetActive(false);
                list.Add(_poolObjects.Last());
            }
            return list;
        }

        public List<GameObject> Initialize(GameObject prefab, int count, Transform parent)
        {
            this._count += count;
            List<GameObject> list = new List<GameObject>();
            for (int i = 0; i < count; i++)
            {
                _poolObjects.Add(Instantiate(prefab, transform.position, Quaternion.identity, parent));
                _poolObjects.Last().SetActive(false);
                list.Add(_poolObjects.Last());
            }
            return list;
        }

        public List<GameObject> GetPoolingObjects() { return _poolObjects; }
    }

    [Serializable]
    public struct PoolInstruction
    {
        public GameObject Prefab;
        public int Count;
    }
}