﻿using Controllers.GameControllers;
using Controllers.Traps;
using UnityEngine;

namespace Extensions.Classes
{
    [RequireComponent(typeof(CheckpointController))]
    public class DebugCheckpoint : MonoBehaviour {

        public KeyCode keyToTeleport;
        CheckpointController _checkpointController;

        void Start()
        {
            _checkpointController = GetComponent<CheckpointController>();
        }

        void Update()
        {
            if (Input.GetKeyDown(keyToTeleport))
            {
                _checkpointController.DebugReset();
                MainGameplayController.LastCheckpointPosition = transform.position;
                MainGameplayController.Instance.RestartFromCheckpoint();
            }
        }
    }
}
