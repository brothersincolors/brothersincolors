﻿using UnityEngine;

namespace Extensions.Classes
{
	public class EnableInputDeviceButtons: MonoBehaviour
	{
		public int PlayerIndex;
		public SpriteRenderer ButtonRenderer;

		private void OnEnable()
		{
			if (Preferences.Prefs.PlayerInputDevices[PlayerIndex] != null)
			{
				if (Preferences.Prefs.PlayerInputDevices[PlayerIndex].GetDeviceName().Contains("PS"))
					transform.GetChild(1).gameObject.SetActive(true);
				else
					transform.GetChild(0).gameObject.SetActive(true);
			}
			else
			{
				transform.GetChild(2).gameObject.SetActive(true);
			}
		}
	
	}
}
