﻿using UnityEngine;

namespace Extensions.Classes
{
    public class LightFixForTiles : MonoBehaviour {

        void Start()
        {
            foreach (Transform child in transform)
            {
                var fixedPositionZ = child.position.z + child.position.x / 10000f + child.position.y / 10000f;
                child.position = child.position + Vector3.forward * fixedPositionZ;
            }
        }
    }
}
