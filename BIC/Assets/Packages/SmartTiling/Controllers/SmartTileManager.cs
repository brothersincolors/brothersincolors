﻿using System;
using Controllers.TilingSystem;
using UnityEngine;

namespace Packages.SmartTiling.Controllers
{
	public class SmartTileManager : MonoBehaviour
	{

		public int Width;
		public int Height;
		
		public float TileSideLength = 1;

		private const int MAX_LAYERS_COUNT = 6;

		private SmartTileController[,,] _grid;
		
		// Properties
		public static SmartTileManager Instance { get; private set; }
		
		// Singleton
		private void Awake() => Instance = this;

		private void Start()
		{
			transform.position = Vector3.zero;
			GenerateMatrix(Width, Height, MAX_LAYERS_COUNT);
		}

		public void GenerateMatrix(int width, int height, int layersCount)
		{
			Width = width = width - width % 2;
			Height = height = height - height % 2;

			var tempGrid = _grid;
			
			_grid = new SmartTileController[width, height, layersCount];
			
			if (tempGrid == null)
				return;
			
			for (var i = 0; i < tempGrid.GetLength(0); i++)
				for (var j = 0; j < tempGrid.GetLength(1); j++)
					for (var layer = 0; layer < tempGrid.GetLength(2); layer++)
						if (tempGrid[i, j, layer] != null)
							AddSmartTile(tempGrid[i, j, layer], tempGrid[i, j, layer].transform.position, 0);
		}

		// Get a 3x3 matrix, describing occupation of neighbouring cells. 1 - cell is occupied, 0 - cell is free
		public byte[,] GetNeighbours(Vector2 tilePosition, int layerIndex, string tag)
		{
			var neighboursMatrix = new byte[3, 3];
			var indexes = GetTileIndexes(tilePosition);
			for (var i = -1; i < 2; i++)
			{
				for (var j = -1; j < 2; j++)
				{
					if (i + indexes.Item1 > 0 && i + indexes.Item1 < Width &&
					    j + indexes.Item2 > 0 && j + indexes.Item2 < Height &&
					    _grid[i + indexes.Item1, j + indexes.Item2, layerIndex] != null &&
					    _grid[i + indexes.Item1, j + indexes.Item2, layerIndex].Tag == tag)
						neighboursMatrix[i + 1, 1 - j] = 1;
				}
			}
			return neighboursMatrix;
		}
		
		public void AddSmartTile(SmartTileController smartTileController, Vector2 tilePosition, int layerIndex)
		{
			var indexes = GetTileIndexes(tilePosition);
			_grid[indexes.Item1, indexes.Item2, layerIndex] = smartTileController;
			UpdateNeighbours(tilePosition, layerIndex);
		}

		public void RemoveSmartTile(Vector2 tilePosition, int layerIndex)
		{
			var indexes = GetTileIndexes(tilePosition);
			_grid[indexes.Item1, indexes.Item2, layerIndex] = null;
			UpdateNeighbours(tilePosition, layerIndex);
		}
		
		private void UpdateNeighbours(Vector2 tilePosition, int layerIndex)
		{
			var indexes = GetTileIndexes(tilePosition);
			for (var i = indexes.Item1 - 1; i < indexes.Item1 + 2; i++)
				for (var j = indexes.Item2 - 1; j < indexes.Item2 + 2; j++)
					if (i > 0 &&  i < Width && j > 0 && j < Height)
						_grid[i, j, layerIndex]?.RecalculateSprite();
		}

		private Tuple<int, int> GetTileIndexes(Vector2 tilePosition)
		{
			var relativePosition = tilePosition - (Vector2) transform.position;
			var indexes = new Tuple<int, int>(
				Width / 2 + (relativePosition.x < 0 ?
					Mathf.FloorToInt(relativePosition.x / TileSideLength):
					(int) (relativePosition.x / TileSideLength)),
				Height / 2 - Mathf.FloorToInt(relativePosition.y / TileSideLength)
			);
			return indexes;
		}

		private void OnDestroy() => Instance = null;
	}
}
