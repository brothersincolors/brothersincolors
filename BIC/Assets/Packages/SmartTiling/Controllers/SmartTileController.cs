﻿using System;
using Packages.LevelEditor.Controllers;
using Packages.SmartTiling.Controllers;
using UnityEngine;

namespace Controllers.TilingSystem
{
    public class SmartTileController : MonoBehaviour {

        public Texture2D Texture;
        public AdittionalTilesPack[] AdittionalTilesPacks;
        public int Layer;
        public string Tag;

        private SpriteRenderer _attRenderer;
        private Sprite[] _tiles = new Sprite[47];

	
        public void RecalculateSprite()
        {
            if (_attRenderer == null)
                _attRenderer = GetComponent<SpriteRenderer>();
            var a = SmartTileManager.Instance.GetNeighbours(transform.position, Layer, Tag);
            var index = 0;
            // One side 1 - 4
            if (a[0, 0] != 0 && a[1, 0] != 0 && a[0, 1] != 0 && a[2, 1] == 0 && a[0, 2] != 0 && a[1, 2] != 0)
                index = 1;
            if (a[0, 1] != 0 && a[2, 1] != 0 && a[0, 2] != 0 && a[1, 2] != 0 && a[2, 2] != 0 && a[1, 0] == 0)
                index = 2;
            if (a[1, 0] != 0 && a[2, 0] != 0 && a[2, 1] != 0 && a[1, 2] != 0 && a[2, 2] != 0 && a[0, 1] == 0)
                index = 3;
            if (a[0, 0] != 0 && a[1, 0] != 0 && a[2, 0] != 0 && a[0, 1] != 0 && a[2, 1] != 0 && a[1, 2] == 0)
                index = 4;
            // Concate corners 5 - 8
            if (a[0, 0] != 0 && a[0, 1] != 0 && a[1, 0] != 0 && a[2, 1] == 0 && a[1, 2] == 0)
                index = 5;
            if (a[2, 1] != 0 && a[2, 2] != 0 && a[1, 2] != 0 && a[0, 1] == 0 && a[1, 0] == 0)
                index = 7;
            if (a[1, 0] == 0 && a[0, 1] != 0 && a[2, 1] == 0 && a[0, 2] != 0 && a[1, 2] != 0)
                index = 6;
            if (a[1, 0] != 0 && a[2, 0] != 0 && a[2, 1] != 0 && a[0, 1] == 0 && a[1, 2] == 0)
                index = 8;
            // Endings 9 - 12
            if (a[0, 1] != 0 && a[1, 0] == 0 && a[1, 2] == 0 && a[2, 1] == 0)
                index = 9;
            if (a[1, 2] != 0 && a[1, 0] == 0 && a[0, 1] == 0 && a[2, 1] == 0)
                index = 10;
            if (a[2, 1] != 0 && a[1, 0] == 0 && a[1, 2] == 0 && a[0, 1] == 0)
                index = 11;
            if (a[1, 0] != 0 && a[0, 1] == 0 && a[1, 2] == 0 && a[2, 1] == 0)
                index = 12;
            // Single block && two sides 13 - 15
            if (a[1, 0] == 0 && a[0, 1] == 0 && a[2, 1] == 0 && a[1, 2] == 0)
                index = 13;
            if (a[0, 1] == 0 && a[2, 1] == 0 && a[1, 0] != 0 && a[1, 2] != 0)
                index = 14;
            if (a[0, 1] != 0 && a[2, 1] != 0 && a[1, 0] == 0 && a[1, 2] == 0)
                index = 15;
            // Convex 1 no side 16 - 19
            if (a[0, 1] != 0 && a[0, 2] != 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[0, 0] != 0 && a[2, 2] == 0 && a[1, 0] != 0 && a[2, 0] != 0)
                index = 16;
            if (a[0, 1] != 0 && a[0, 2] != 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[0, 0] != 0 && a[2, 2] != 0 && a[1, 0] != 0 && a[2, 0] == 0)
                index = 17;
            if (a[0, 1] != 0 && a[0, 2] != 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[0, 0] == 0 && a[2, 2] != 0 && a[1, 0] != 0 && a[2, 0] != 0)
                index = 18;
            if (a[0, 1] != 0 && a[0, 2] == 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[0, 0] != 0 && a[2, 2] != 0 && a[1, 0] != 0 && a[2, 0] != 0)
                index = 19;
            // Convex 2 no side 20 - 23
            if (a[0, 1] != 0 && a[0, 2] != 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[0, 0] != 0 && a[2, 2] == 0 && a[1, 0] != 0 && a[2, 0] == 0)
                index = 20;
            if (a[0, 1] != 0 && a[0, 2] != 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[0, 0] == 0 && a[2, 2] != 0 && a[1, 0] != 0 && a[2, 0] == 0)
                index = 21;
            if (a[0, 1] != 0 && a[0, 2] == 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[0, 0] == 0 && a[2, 2] != 0 && a[1, 0] != 0 && a[2, 0] != 0)
                index = 22;
            if (a[0, 1] != 0 && a[0, 2] == 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[0, 0] != 0 && a[2, 2] == 0 && a[1, 0] != 0 && a[2, 0] != 0)
                index = 23;
            // Convex 3 no side 24 - 27
            if (a[0, 1] != 0 && a[0, 2] == 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[0, 0] != 0 && a[2, 2] == 0 && a[1, 0] != 0 && a[2, 0] == 0)
                index = 24;
            if (a[0, 1] != 0 && a[0, 2] != 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[0, 0] == 0 && a[2, 2] == 0 && a[1, 0] != 0 && a[2, 0] == 0)
                index = 25;
            if (a[0, 1] != 0 && a[0, 2] == 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[0, 0] == 0 && a[2, 2] != 0 && a[1, 0] != 0 && a[2, 0] == 0)
                index = 26;
            if (a[0, 1] != 0 && a[0, 2] == 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[0, 0] == 0 && a[2, 2] == 0 && a[1, 0] != 0 && a[2, 0] != 0)
                index = 27;
            // Convex 4 no side && Covex 2 diagonal 45, 46
            if (a[0, 1] != 0 && a[0, 2] == 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[1, 0] != 0 && a[0, 0] == 0 && a[2, 2] == 0 && a[1, 0] != 0 && a[2, 0] == 0)
                index = 28;
            if (a[0, 1] != 0 && a[0, 2] != 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[1, 0] != 0 && a[0, 0] == 0 && a[2, 2] == 0 && a[1, 0] != 0 && a[2, 0] != 0)
                index = 45;
            if (a[0, 1] != 0 && a[0, 2] == 0 && a[1, 2] != 0 && a[2, 1] != 0 && a[1, 0] != 0 && a[0, 0] != 0 && a[2, 2] != 0 && a[1, 0] != 0 && a[2, 0] == 0)
                index = 46;
            // Convex 1 1 side 29 - 36
            if (a[1, 0] != 0 && a[2, 0] != 0 && a[2, 1] != 0 && a[1, 2] != 0 && a[2, 2] == 0 && a[0, 1] == 0)
                index = 29;
            if (a[0, 0] != 0 && a[1, 0] != 0 && a[2, 0] == 0 && a[0, 1] != 0 && a[2, 1] != 0 && a[1, 2] == 0)
                index = 30;
            if (a[0, 0] == 0 && a[1, 0] != 0 && a[0, 1] != 0 && a[2, 1] == 0 && a[0, 2] != 0 && a[1, 2] != 0)
                index = 31;
            if (a[1, 0] == 0 && a[0, 1] != 0 && a[2, 1] != 0 && a[0, 2] == 0 && a[1, 2] != 0 && a[2, 2] != 0)
                index = 32;
            if (a[1, 0] == 0 && a[0, 1] != 0 && a[2, 1] != 0 && a[0, 2] != 0 && a[1, 2] != 0 && a[2, 2] == 0)
                index = 33;
            if (a[1, 0] != 0 && a[2, 0] == 0 && a[0, 1] == 0 && a[2, 1] != 0 && a[1, 2] != 0 && a[2, 2] != 0)
                index = 34;
            if (a[0, 0] == 0 && a[1, 0] != 0 && a[2, 0] != 0 && a[0, 1] != 0 && a[2, 1] != 0 && a[1, 2] == 0)
                index = 35;
            if (a[0, 0] != 0 && a[1, 0] != 0 && a[0, 1] != 0 && a[2, 1] == 0 && a[0, 2] == 0 && a[1, 2] != 0)
                index = 36;
            // Convex 1 side 2 37 - 40
            if (a[1, 0] == 0 && a[0, 1] == 0 && a[2, 1] != 0 && a[1, 2] != 0 && a[2, 2] == 0)
                index = 37;
            if (a[1, 0] != 0 && a[2, 0] == 0 && a[0, 1] == 0 && a[2, 1] != 0 && a[1, 2] == 0)
                index = 38;
            if (a[1, 0] == 0 && a[0, 1] != 0 && a[2, 1] == 0 && a[0, 2] == 0 && a[1, 2] != 0)
                index = 39;
            if (a[0, 0] == 0 && a[1, 0] != 0 && a[0, 1] != 0 && a[2, 1] == 0 && a[1, 2] == 0)
                index = 40;
            // Convex 2 side 1 41 - 44
            if (a[1, 0] != 0 && a[2, 0] == 0 && a[0, 1] == 0 && a[2, 1] != 0 && a[1, 2] != 0 && a[2, 2] == 0)
                index = 41;
            if (a[0, 0] == 0 && a[1, 0] != 0 && a[2, 0] == 0 && a[0, 1] != 0 && a[2, 1] != 0 && a[1, 2] == 0)
                index = 42;
            if (a[0, 0] == 0 && a[1, 0] != 0 && a[0, 1] != 0 && a[2, 1] == 0 && a[0, 2] == 0 && a[1, 2] != 0)
                index = 43;
            if (a[1, 0] == 0 && a[0, 1] != 0 && a[2, 1] != 0 && a[0, 2] == 0 && a[1, 2] != 0 && a[2, 2] == 0)
                index = 44;
            
            
            AdittionalTilesPack additionalTilesPack = null;
            foreach (var tilePack in AdittionalTilesPacks)
                if (tilePack.Index == index)
                    additionalTilesPack = tilePack;

            if (additionalTilesPack != null)
            {
                var localIndex = UnityEngine.Random.Range(0, additionalTilesPack.Sprites.Length + 1);
                _attRenderer.sprite = localIndex == 0 ? _tiles[index] : additionalTilesPack.Sprites[localIndex - 1];
            }
            else
                _attRenderer.sprite = _tiles[index];

        }

        private void Start () {
            if (!SmartTileManager.Instance) return;
            
            _tiles = Resources.LoadAll<Sprite>("SmartTileTextures/" + Texture.name);
            _attRenderer = GetComponent<SpriteRenderer>();

            Layer = LevelEditorController.Instance.GetLayerIndex(transform.parent.gameObject);
            SmartTileManager.Instance.AddSmartTile(this, transform.position, Layer);
        }

        private void OnDestroy()
        {
            if (SmartTileManager.Instance != null)
                SmartTileManager.Instance.RemoveSmartTile(transform.position, Layer);
        }

        [Serializable]
        public class AdittionalTilesPack
        {
            public int Index;
            public Sprite[] Sprites;
        }
    }
}
