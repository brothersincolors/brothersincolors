﻿using System.Collections.Generic;
using UnityEngine;
using InputControl.DeviceClass;
using System;
using DataBases.KeycodesDataBase;

namespace InputControl
{
    public class InputDevice
    {
        private Device connectedDevice;     // Геймпад, который подключен к этому InputDevice
        private bool deviceIsConnected;     // Подкулючен ли к нам геймпад?
        private KeycodeConverter keyConverter;      // Конвертирует инпут. Подробнее внутри него
        private Device previousDevice;      // Предидущий геймпад

        public bool DeviceIsConnected
        {
            get
            {
                return deviceIsConnected;
            }
        }

        Device ConnectedDevice      // Свойсто для работы с геймпадом. 
        {
            get { return connectedDevice; }
            set
            {
                previousDevice = connectedDevice;   // Запоминаем предидущий геймпад
                connectedDevice = value;        // Берем новый геймпад
                deviceIsConnected = value != null;      // Если нового неймпада нет, то говорим, что к нам не подключен ни один геймпад
                if (connectedDevice != null)        // Если геймпад подключен, то делаем для него конвертер инпута       
                    keyConverter = new KeycodeConverter(connectedDevice.Name);
                else
                    keyConverter = new KeycodeConverter(null);
            }
        }

        public InputDevice()
        {
            ConnectedDevice = InputManager.GetFreeDevice();    
            InputManager.OnJoysticksUpdated += OnJoysticksUpdated;
        }

        public InputDevice(Device _device)      // Создаем InputDevice и даем ему сразу какой-то геймпад
        {
            if (!_device.isUsed)         // Если геймпад, который мы ему даем не занят, то используем его       
            {
                ConnectedDevice = _device;
                _device.isUsed = true;
            } else
            {
                ConnectedDevice = InputManager.GetFreeDevice();
            }
            InputManager.OnJoysticksUpdated += OnJoysticksUpdated;
        }

        ~InputDevice()
        {
            InputManager.OnJoysticksUpdated -= OnJoysticksUpdated;
        }

        public void FreeDevice()        // Метод, который "удаляет" InputDevice.
        {
            InputManager.OnJoysticksUpdated -= OnJoysticksUpdated;
            if (connectedDevice == null) return;
            connectedDevice.isUsed = false;
        }

        public string GetDeviceName()   // Получаем имя геймпада, к которому прикреплены
        {
            if (deviceIsConnected)
                return ConnectedDevice.Name;
            else
                Debug.Log("Device is not attached.");
            return "";
        }

        public bool CompareDevices(Device device) => ConnectedDevice == device;

        private void OnJoysticksUpdated(List<Device> devices)   // Вызывается при обновлении списка
        {
            if (connectedDevice != null)    // Если геймпад прикреплен
            {
                if (!devices.Contains(connectedDevice))     // Проверяем, есть ли он в новом списке
                {                                           // Если нет, то ищем новый геймпад
                    ConnectedDevice = InputManager.GetFreeDevice(connectedDevice);
                }
            } else      // Если нет, то ищем новый геймпад 
            {
                ConnectedDevice = InputManager.GetFreeDevice(previousDevice);       
            }
        }

        // Get input

        public bool GetKeyDown(GamepadKeycode keycode)
        {
            if (connectedDevice == null) return false;
            int buttonIndex = keyConverter.ConvertKeycode((int)keycode);    // Конвертируем инпут
            // Получаем имя кнопки
            string keyName = "joystick" + (((InputManager.ConnectedDevicesCount == 1) ? "" : " " + (connectedDevice.index + 1).ToString())  + " button " + buttonIndex);
            // Возвращаем занчение кнопки
            return Input.GetKeyDown(keyName);
        }
        public bool GetKeyUp(GamepadKeycode keycode)
        {
            if (connectedDevice == null) return false;
            int buttonIndex = keyConverter.ConvertKeycode((int)keycode);
            string keyName = "joystick" + (((InputManager.ConnectedDevicesCount == 1) ? "" : " " + (connectedDevice.index + 1).ToString()) + " button " + buttonIndex);
            return Input.GetKeyUp(keyName);
        }
        public bool GetKey(GamepadKeycode keycode)
        {
            if (connectedDevice == null) return false;
            int buttonIndex = keyConverter.ConvertKeycode((int)keycode);
            string keyName = "joystick" + (((InputManager.ConnectedDevicesCount == 1) ? "" : " " + (connectedDevice.index + 1).ToString()) + " button " + buttonIndex);
            return Input.GetKey(keyName);
        }

        public float GetAxis(GamepadKeycode axis)
        {
            if (connectedDevice == null) return 0;
            int axisIndex = keyConverter.ConvertKeycode((int)axis);     // Конвертим инпут
            // Возвращаем значение оси
            return Input.GetAxis("joystick " + (connectedDevice.index + 1).ToString() + " analog " + (axisIndex - 16));
        }

        public float GetAxisRaw(GamepadKeycode axis)
        {
            if (connectedDevice == null) return 0;
            int axisIndex = keyConverter.ConvertKeycode((int)axis);     // Конвертим инпут
            // Возвращаем значение оси
            return Input.GetAxisRaw("joystick " + (connectedDevice.index + 1).ToString() + " analog " + (axisIndex - 16));
        }
    }
}