﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DataBases.KeycodesDataBase;

namespace InputControl
{
	using DeviceClass;

	public class InputManager : MonoBehaviour
    {

        public bool logDebugInfo;       // Вывод логов

        private static  int connectedDevicesCount = 0;      // Кол-во подключенных геймпадов
        private Coroutine updateJoysticks;

        public static int ConnectedDevicesCount
        {
            get
            {
                return connectedDevicesCount;
            }
        }

        // Devices data
        static List<Device> connectedDevices = new List<Device>();      // Лист с подключенными геймпадами

        // Events
        public delegate void OnDeviceEvent(Device device);
        public delegate void OnJoysticksEvent(List<Device> devices);

        public static event OnDeviceEvent OnJoystickDisconnected;       // Вызывается при подключении геймпада
        public static event OnDeviceEvent OnJoystickConnected;          // Вызывается при отключении геймпада
        public static event OnJoysticksEvent OnJoysticksUpdated;        // Вызывается каждое обновление списка геймпадов

        public static Device GetFreeDevice()        // Находит первый свободный геймпад в списке
        {
            var device = connectedDevices.Find((Device obj) => { return !obj.isUsed; });
            if (device != null)
                device.isUsed = true;
            return device;
        }

        public static Device GetFreeDevice(Device dev)      // Отдает первый девайс с таким же именем
        {
            if (dev == null) return GetFreeDevice();
            var device = connectedDevices.Find((Device obj) => { return !obj.isUsed && dev.Name == obj.Name; });
            if (device != null)
                device.isUsed = true;
            else
                device = GetFreeDevice();
            return device;
        }

		public static KeyCode GetKeycodeRuntime()       // Возвращает keycode нажатой клавиши
		{
			KeyCode key = KeyCode.None;

			foreach (KeyCode button in Enum.GetValues(typeof(KeyCode)))
			{
				if (Input.GetKey(button))
				{
                    return button;
				}
			}
			return key;
		}

        public static Device[] GetConnectedDevices()        // Возвращает лист подключенных геймпадов
        {
            UpdateConnectedJoysticks();
            Device[] devices = new Device[connectedDevices.Count];
            for (int i = 0; i < devices.Length; i++)
            {
                devices[i] = connectedDevices[i];
            }
            return devices;
        }

        private void Awake()
        {
            OnJoystickConnected += JoystickConnected;
            OnJoystickDisconnected += JoystickDisconnected;
        }

        private void Start()
        {
            updateJoysticks = StartCoroutine(UpdateJoysticks(0.5f));        // Запускаем обновление геймпадов
        }

        private static IEnumerator UpdateJoysticks(float updateTime)
        {
            while (true)
            {
                UpdateConnectedJoysticks();
                yield return new WaitForSeconds(updateTime);
            }
        }

        private static void UpdateConnectedJoysticks()
        {
            var joysticks = Input.GetJoystickNames();       // Получаем массив имен джойстиков
            List<Device> connectedDevicesCopy = new List<Device>(connectedDevices.ToArray());       // Копия нынешних геймпадов
            List<Device> currentConntectedDevices = new List<Device>();     // Пустой лист для хранения новых геймпадов
            for (int i = 0; i < joysticks.Length; i++)
            {
                Device newDevice = new Device(joysticks[i], i);     // Создаем новые геймпады, и добавляем в лист
                currentConntectedDevices.Add(newDevice);
            }
            foreach (var device in currentConntectedDevices)    // Если мы не находим геймпад в нашем списке, добавляем его
            {
                if (connectedDevices.Find(delegate (Device obj) {
                    return obj.index == device.index && obj.Name == device.Name;
                }) == null)
                {
                    connectedDevices.Add(device);
                }
            }
            foreach (var device in connectedDevicesCopy)    // Если какой-нибудь геймпад отсутствует в списке, удалаем его
            {
                if (currentConntectedDevices.Find(delegate (Device obj) {
                    return (obj.index == device.index && obj.Name == device.Name);
                }) == null)
                {
                    connectedDevices.Remove(device);
                }
            }
            foreach (var device in connectedDevices)        // Вызываем ивенты на добавление геймпада
            {
                if (!connectedDevicesCopy.Contains(device))
                    OnJoystickConnected(device);
            }
            foreach (var device in connectedDevicesCopy)    // Выщываем ивенты на отключение геймпада
            {
                if (!connectedDevices.Contains(device))
                    OnJoystickDisconnected(device);
            }
            if (OnJoysticksUpdated != null)                 // Вызываем обновление списка
                OnJoysticksUpdated(connectedDevices);
            connectedDevicesCount = connectedDevices.Count;     // Обновляем кол-во
        }

        private void JoystickDisconnected(Device device)    // Логи
        {

            if (logDebugInfo)
            {
                Debug.Log("Joystick disconnected: " + device.Name + " at index " + device.index);
            }
        }

        private void JoystickConnected(Device device)       // Логи
        {
			if (logDebugInfo)
			{
                Debug.Log("Joystick connected: " + device.Name + " at index " + device.index);
			}
        }


        public static class GlobalJoystick
        {
            /* Возвращает действия всех геймпадов */

            public static bool GetKey(GamepadKeycode keycode)
            {
                Device temp = null;
                return GetKey(keycode, ref temp);
            }

            public static bool GetKeyDown(GamepadKeycode keycode)
            {
                Device temp = null;
                return GetKeyDown(keycode, ref temp);
            }

            public static bool GetKeyUp(GamepadKeycode keycode)
            {
                Device temp = null;
                return GetKeyUp(keycode, ref temp);
            }

            public static bool GetKey(GamepadKeycode keycode, ref Device device)
            {
                for (int i = 0; i < connectedDevices.Count; i++)    // Проходим по всем геймпадам и проверяем нажатие на клавишу
                {
                    if (connectedDevices[i].isCalibrating) continue;
                    var keyConverter = new KeycodeConverter(connectedDevices[i].Name);
                    int buttonIndex = keyConverter.ConvertKeycode((int)keycode);
                    string keyName = "joystick" + (((ConnectedDevicesCount == 1) ? "" : " " + (connectedDevices[i].index + 1).ToString()) + " button " + buttonIndex);
                    if (Input.GetKey(keyName))
                    {
                        device = connectedDevices[i];
                        return true;
                    }
                }
                return false;
            }

            public static bool GetKeyDown(GamepadKeycode keycode, ref Device device)
            {
                for (int i = 0; i < connectedDevices.Count; i++)
                {
                    if (connectedDevices[i].isCalibrating) continue;
                    var keyConverter = new KeycodeConverter(connectedDevices[i].Name);
                    int buttonIndex = keyConverter.ConvertKeycode((int)keycode);
                    string keyName = "joystick" + (((ConnectedDevicesCount == 1) ? "" : " " + (connectedDevices[i].index + 1).ToString()) + " button " + buttonIndex);
                    if (Input.GetKeyDown(keyName))
                    {
                        device = connectedDevices[i];
                        return true;
                    }
                }
                device = null;
                return false;
            }

            public static bool GetKeyUp(GamepadKeycode keycode, ref Device device)
            {
                for (int i = 0; i < connectedDevices.Count; i++)
                {
                    if (connectedDevices[i].isCalibrating) continue;
                    var keyConverter = new KeycodeConverter(connectedDevices[i].Name);
                    int buttonIndex = keyConverter.ConvertKeycode((int)keycode);
                    string keyName = "joystick" + (((ConnectedDevicesCount == 1) ? "" : " " + (connectedDevices[i].index + 1).ToString()) + " button " + buttonIndex);
                    if (Input.GetKeyUp(keyName))
                    {
                        device = connectedDevices[i];
                        return true;
                    }
                }
                return false;
            }

            public static bool GetJoystickButtonDown(out int button)    // Возвращает Keycode кнопки в button
            {
                for (int i = 0; i < connectedDevices.Count; i++)
                {
                    for (int j = 0; j < 17; j++)
                    {
                        string keyName = "joystick" + (((ConnectedDevicesCount == 1) ? "" : " " + (connectedDevices[i].index + 1).ToString()) + " button " + j);
                        if (Input.GetKeyDown(keyName))
                        {
                            button = j;
                            return true;
                        }
                    }
                }
                button = -1;
                return false;
            }

            public static bool GetJoystickButtonDown(out int button, out Device device)     // Возвращает Keycode кнопки в button и геймпад в device
            {
                for (int i = 0; i < connectedDevices.Count; i++)
                {
                    for (int j = 0; j < 17; j++)
                    {
                        string keyName = "joystick" + (((ConnectedDevicesCount == 1) ? "" : " " + (connectedDevices[i].index + 1).ToString()) + " button " + j);
                        if (Input.GetKeyDown(keyName))
                        {
                            button = j;
                            device = connectedDevices[i];
                            return true;
                        }
                    }
                }
                button = int.MinValue;
                device = null;
                return false;
            }

            public static bool GetJoystickAxis(out int axis, out Device device)     // Возвращает Keycode оси в axis и геймпад в device
            {
                for (int i = 0; i < connectedDevicesCount; i++)
                {
                    for (int j = 0; j < 20; j++)
                    {
                        if (Math.Abs(Input.GetAxisRaw("joystick " + (connectedDevices[i].index + 1).ToString() + " analog " + j)) > 0.8f)
                        {
                            device = connectedDevices[i];
                            axis = 16 + j;
                            return true;
                        }
                    }
                }
                device = null;
                axis = int.MinValue;
                return false;
            }

            public static float GetAxis(GamepadKeycode axis)    // Возвращает ось со всех геймпадов
            {
                for (int i = 0; i < connectedDevices.Count; i++)
                {
                    string axisName = "joystick " + (connectedDevices[i].index + 1).ToString() + " analog " + ((int)axis - 16);
                    var val = Input.GetAxis(axisName);
                    if (val != 0)
                    {
                        return val;
                    }
                }
                return 0;
            }

            public static float GetAxis(GamepadKeycode axis, float deadzone)        // Возвращает ось со всех геймпадо с учетом deadzone
            {
                for (int i = 0; i < connectedDevices.Count; i++)
                {
                    string axisName = "joystick " + (connectedDevices[i].index + 1).ToString() + " analog " + ((int)axis - 16);
                    var val = Input.GetAxis(axisName);
                    if (Mathf.Abs(val) >= Mathf.Abs(deadzone))
                    {
                        return val;
                    }
                }
                return 0;
            }

            public static float GetAxisRaw(GamepadKeycode axis)     // Возвращает ось со всех геймпадов
            {
                for (int i = 0; i < connectedDevices.Count; i++)
                {
                    string axisName = "joystick " + (connectedDevices[i].index + 1).ToString() + " analog " + ((int)axis - 16);
                    var val = Input.GetAxisRaw(axisName);
                    if (val != 0)
                    {
                        return val;
                    }
                }
                return 0;
            }
        }

        public static class Keyboard
        {
            public static bool GetKey(KeyCode keyCode)
            {
                return Input.GetKey(keyCode);
            }

            public static bool GetKeyDown(KeyCode keyCode)
            {
                return Input.GetKeyDown(keyCode);
            }

            public static bool GetKeyUp(KeyCode keyCode)
            {
                return Input.GetKeyUp(keyCode);
            }
        }

    }

    namespace DeviceClass
    {
        [Serializable]
        public class Device     
        {
            /* Класс, который хранит в себе данные о геймпаде */
            public int index;       // Индекс геймпада
            public bool isUsed;     // Используется ли геймпад
            public bool isCalibrating;      // Калибруется ли геймпад

            private string name;        // Имя геймпада

            public string Name
            {
                get
                {
                    return name;
                }
            }

            public Device(string name)
            {
                this.name = name;
            }

            public Device(string name, int index)
            {
                this.name = name;
                this.index = index;
            }

            public bool EqualsTo(Device device)
            {
                if (device == null) return false;
                if (device.index == index && device.name == name && device.isUsed == isUsed && device.isCalibrating == isCalibrating) return true;
                return false;
            }
        }
    }

    public enum InputDeviceType { Gamepad, Keyboard }
}