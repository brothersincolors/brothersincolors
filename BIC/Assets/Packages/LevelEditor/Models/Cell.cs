﻿using System.Collections.Generic;
using Packages.LevelEditor.Controllers;
using UnityEngine;

namespace Packages.LevelEditor.Models
{
	public class Cell
	{

		public readonly Vector3 Position;
		public readonly float SideLength;

		public Dictionary<int, PlaceableObjectController> PlacedObjectControllers = new Dictionary<int, PlaceableObjectController>();

		public Cell(Vector3 position, float sideLength)
		{
			SideLength = sideLength;
			Position = position + new Vector3(SideLength / 2, SideLength / 2);
		}

		public void SpawnPlaceableObject(PlaceableObjectController placeableObjectController, int layerIndex)
		{
			var placedObject = Object.Instantiate(placeableObjectController, Position, Quaternion.identity);
			placedObject.gameObject.transform.SetParent(LevelEditorController.Instance.GetLayerHolder(layerIndex)?.transform);
			
			RemovePlaceableObject(layerIndex);
			if (PlacedObjectControllers.ContainsKey(layerIndex))
				PlacedObjectControllers[layerIndex] = placedObject;
			else
				PlacedObjectControllers.Add(layerIndex, placedObject);
		}
		
		public void SetPlacedObject(PlaceableObjectController placedObject, int layerIndex)
		{
			RemovePlaceableObject(layerIndex);
			if (PlacedObjectControllers.ContainsKey(layerIndex))
				PlacedObjectControllers[layerIndex] = placedObject;
			else
				PlacedObjectControllers.Add(layerIndex, placedObject);
		}

		public void RemovePlaceableObject(int layerIndex)
		{
			if (!PlacedObjectControllers.ContainsKey(layerIndex))
				return;
			PlaceableObjectController currentPlacedObject;
			if (!PlacedObjectControllers.TryGetValue(layerIndex, out currentPlacedObject)) return;
			if (currentPlacedObject)
				Object.DestroyImmediate(currentPlacedObject.gameObject);
		}
		
		// TODO: highlight cell
//		if (_selectedCell != null)
//		{
//			GL.Vertex3(_selectedCell.Position.x - _cellSide / 2, _selectedCell.Position.y - _cellSide / 2, 0);
//			GL.Vertex3(_selectedCell.Position.x + _cellSide / 2, _selectedCell.Position.y - _cellSide / 2, 0);
//				
//			GL.Vertex3(_selectedCell.Position.x - _cellSide / 2, _selectedCell.Position.y - _cellSide / 2, 0);
//			GL.Vertex3(_selectedCell.Position.x - _cellSide / 2, _selectedCell.Position.y + _cellSide / 2, 0);
//				
//			GL.Vertex3(_selectedCell.Position.x + _cellSide / 2, _selectedCell.Position.y - _cellSide / 2, 0);
//			GL.Vertex3(_selectedCell.Position.x + _cellSide / 2, _selectedCell.Position.y + _cellSide / 2, 0);
//				
//			GL.Vertex3(_selectedCell.Position.x - _cellSide / 2, _selectedCell.Position.y + _cellSide / 2, 0);
//			GL.Vertex3(_selectedCell.Position.x + _cellSide / 2, _selectedCell.Position.y + _cellSide / 2, 0);
//		}
	}
}
