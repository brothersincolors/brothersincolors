﻿using Packages.LevelEditor.Views;
using UnityEngine;
using UnityEngine.UI;

namespace Packages.LevelEditor.Models
{
	[RequireComponent(typeof(Button))]
	public class Tab : MonoBehaviour
	{

		public GameObject TabContent;
		public TabView TabView;

		private void Start()
		{
			GetComponent<Button>().onClick.AddListener(() =>
			{
				TabView.UpdateContentView(TabContent);
			});
		}
	}
}
