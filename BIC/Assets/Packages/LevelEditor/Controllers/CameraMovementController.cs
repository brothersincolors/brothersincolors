﻿using System;
using UnityEngine;

namespace Packages.LevelEditor.Controllers
{
	public class CameraMovementController : MonoBehaviour
	{

		public float MinHeight = 1;
		
		private Vector3 _viewportStartPoint;
		private Vector3 _cameraWorldStartPoint;
		
		
		public void Move()
		{
			var viewportCurrentPoint = Camera.main.ScreenToViewportPoint(Input.mousePosition);
			var worldOffset = Camera.main.ViewportToWorldPoint(viewportCurrentPoint)
			                   - Camera.main.ViewportToWorldPoint(_viewportStartPoint);
			transform.position = _cameraWorldStartPoint - worldOffset;
		}
		
		private void Update()
		{
			if (Input.GetMouseButtonDown(1))
			{
				_viewportStartPoint = Camera.main.ScreenToViewportPoint(Input.mousePosition);
				_cameraWorldStartPoint = transform.position;
			}
			
			if (Input.GetMouseButton(1))
				Move();
			
			Camera.main.orthographicSize = Mathf.Max(MinHeight, Camera.main.orthographicSize + Input.mouseScrollDelta.y / 10);
		}
		
	}
}
