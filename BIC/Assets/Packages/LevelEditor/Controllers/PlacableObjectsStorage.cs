﻿using Boo.Lang;
using UnityEngine;

namespace Packages.LevelEditor.Controllers
{
	public class PlacableObjectsStorage : MonoBehaviour
	{

		public List<PlaceableObjectController> Terrain = new List<PlaceableObjectController>();
		public List<PlaceableObjectController> Nature = new List<PlaceableObjectController>();
		public List<PlaceableObjectController> Furniture = new List<PlaceableObjectController>();
		public List<PlaceableObjectController> Traps = new List<PlaceableObjectController>();
		
		public static PlacableObjectsStorage Instance { get; private set; }

		private void Awake() => Instance = this;
	}
}
