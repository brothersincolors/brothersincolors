﻿using System.Runtime.InteropServices;
using Packages.LevelEditor.Models;
using UnityEngine;
using UnityEngine.Rendering;
using UnityScript.Macros;

namespace Packages.LevelEditor.Controllers
{
	public class GridController
	{
		private readonly Color _gridColor;
		private readonly Color _selectedCellColor;

		private Vector2 _gridStartPoint;

		private int _horizontalLinesCount;
		private int _verticalLinesCount;
		
		private float _horizontalLineLength;
		private float _verticalLineLength;

		private float _cellSide;
		
		private Cell[,] _grid;
		private Material _lineMaterial;

		private Cell _selectedCell;

		public GridController(int widthInCells, int heightInCells, float cellSide, Color gridColor)
		{
			_gridColor = gridColor;
			GenerateGrid(widthInCells, heightInCells, cellSide);
			CreateLineMaterial();
		}

		public Cell GetCell(Vector2 relativePoint)
		{
			var xIndex = (_verticalLinesCount - 1) / 2 + (relativePoint.x < 0 ? 
				             Mathf.FloorToInt(relativePoint.x / _cellSide):
				             (int)(relativePoint.x / _cellSide));
			var yIndex = (_horizontalLinesCount - 1) / 2 - Mathf.FloorToInt(relativePoint.y / _cellSide);
			if (xIndex > _verticalLinesCount - 2 || xIndex < 0 ||
			    yIndex > _horizontalLinesCount - 2 || yIndex < 0)
				return null;
			return _grid[xIndex, yIndex];
		}

		public void SelectCell(Cell cellToSelect) => _selectedCell = cellToSelect;
		public void DeselectCell() => _selectedCell = null;
				
		private void GenerateGrid(int widthInCells, int heightInCells, float cellSide)
		{
			_grid = new Cell[widthInCells, heightInCells];
			var xOffset = -widthInCells / 2 * cellSide;
			var yOffset = heightInCells / 2 * cellSide;
			_gridStartPoint = new Vector2(xOffset, yOffset + cellSide);
			for (var i = 0; i < widthInCells; i++)
			{
				yOffset = heightInCells / 2 * cellSide;
				for (var j = 0; j < heightInCells; j++)
				{
					_grid[i, j] = new Cell(new Vector3(xOffset, yOffset), cellSide);
					yOffset -= cellSide;
				}
				xOffset += cellSide;
			}

			_horizontalLinesCount = heightInCells + 1;
			_verticalLinesCount = widthInCells + 1;

			_horizontalLineLength = widthInCells * cellSide;
			_verticalLineLength = heightInCells * cellSide;

			_cellSide = cellSide;
		}
		
		private void CreateLineMaterial()
		{
			var shader = Shader.Find("Hidden/Internal-Colored");
			_lineMaterial = new Material(shader);
			_lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			_lineMaterial.SetInt("_Cull", (int) CullMode.Off);
			_lineMaterial.SetInt("_ZWrite", 0);
			_lineMaterial.color = _gridColor;
		}
		
		// Drawing methods
		public void DrawGrid(Transform transform)
		{
			_lineMaterial.SetPass(0);
			GL.PushMatrix();
			GL.MultMatrix(transform.localToWorldMatrix);
			GL.Begin(GL.LINES);
			
			for (var i = 0; i < _horizontalLinesCount; i++)
			{
				GL.Vertex3(_gridStartPoint.x, _gridStartPoint.y - _cellSide * i, 0);
				GL.Vertex3(_gridStartPoint.x + _horizontalLineLength,  _gridStartPoint.y - _cellSide * i, 0);
			}
			for (var i = 0; i < _verticalLinesCount; i++)
			{
				GL.Vertex3(_gridStartPoint.x + _cellSide * i, _gridStartPoint.y, 0);
				GL.Vertex3(_gridStartPoint.x + _cellSide * i, _gridStartPoint.y - _verticalLineLength, 0);
			}
			
			GL.End();
			GL.PopMatrix();
		}
	}
}
