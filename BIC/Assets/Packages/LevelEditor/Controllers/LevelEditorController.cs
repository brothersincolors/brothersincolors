﻿using System;
using System.Runtime.InteropServices;
using Packages.LevelEditor.Models;
using TMPro;
using UnityEngine;

namespace Packages.LevelEditor.Controllers
{
	public class LevelEditorController : MonoBehaviour {
		
		public int WidthInCells = 100;
		public int HeightInCells = 100;
		
		public int CellSide = 1;

		public Color GridColor = Color.white;

		public string LevelName = "temp_level";

		private GridController _gridController;
		private bool _drawGrid = true;

		// Layers
		public const int MAX_LAYERS_COUNT = 6;
		
		// Placeable objects
		private PlaceableObjectController _selectedObject;
		
		// Properties
		public static LevelEditorController Instance { get; private set; }

		public GameObject LevelHolder { get; private set; }
		
		public int CurrentLayer { get; private set; }

		//Singleton
		private void Awake() => Instance = this;

		// Initialize stuff
		private void Start()
		{
			WidthInCells = WidthInCells - WidthInCells % 2;
			HeightInCells = HeightInCells - HeightInCells % 2;
			_gridController = new GridController(WidthInCells, HeightInCells, CellSide, GridColor);
			LevelHolder = new GameObject {name = LevelName};
			// Initialize layers
			for (var i = 0; i < MAX_LAYERS_COUNT; i++)
			{
				var layer = new GameObject {name = "layer " + i};
				layer.transform.SetParent(LevelHolder.transform);
				layer.transform.localPosition = Vector3.zero;
			}
		}
		// Input
		private void Update()
		{
			if (Input.GetMouseButton(0) && (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)))
			{
				if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
					RemoveObject(Camera.main.ScreenToWorldPoint(Input.mousePosition));
				else
					PlaceObject(Camera.main.ScreenToWorldPoint(Input.mousePosition));
			}
			
			if (Input.GetKeyDown(KeyCode.Equals) || Input.GetKeyDown(KeyCode.Plus))
				IncrementLayer();
			
			if (Input.GetKeyDown(KeyCode.Underscore) || Input.GetKeyDown(KeyCode.Minus))
				DecrementLayer();
		}
		// Draw grid TODO:add show/hide grid mode
		private void OnRenderObject()
		{
			if (LevelHolder != null && _drawGrid)
				_gridController.DrawGrid(LevelHolder.transform);
		}
		
		// Working with level GameObject
		public GameObject GetLayerHolder(int layerIndex)
		{
			foreach (Transform layer in LevelHolder.transform)
			{
				if (layer.name == "layer " + layerIndex)
					return layer.gameObject;
			}

			return null;
		}

		public int GetLayerIndex(GameObject layerHolder)
		{
			return int.Parse(layerHolder.name.Split(' ')[1]);
		}
		
		// Functionality
		private void PlaceObject(Vector2 mousePosition)
		{
			if (!_selectedObject) return;
			var cell = _gridController.GetCell(mousePosition - (Vector2) LevelHolder.transform.position);
			if (cell == null)
				return;
			
			cell.SpawnPlaceableObject(_selectedObject, CurrentLayer);
			_gridController.DeselectCell();
			_gridController.SelectCell(cell);
		}

		public void SetSelectedObject(PlaceableObjectController placeableObjectController) =>
			_selectedObject = placeableObjectController;

		private void RemoveObject(Vector2 mousePoistion)
		{
			var cell = _gridController.GetCell(mousePoistion - (Vector2) LevelHolder.transform.position);
			cell.RemovePlaceableObject(CurrentLayer);
		}
		
		public void IncrementLayer() => CurrentLayer = Math.Min(CurrentLayer + 1, MAX_LAYERS_COUNT - 1);

		public void DecrementLayer() => CurrentLayer = Math.Max(CurrentLayer - 1, 0);

		public void RegenerateGrid(int width, int height, int cellSide)
		{
			WidthInCells = width = width - width % 2;
			HeightInCells = height = height - height % 2;
			_gridController = new GridController(width, height, cellSide, GridColor);
			UpdateCellsContent();
		}

		public void HideShowGrid() => _drawGrid = !_drawGrid;

		public void LoadLevel(string levelName)
		{
			if (levelName == "")
			{
				Debug.LogWarning("Level name not stated!");
				return;
			}
			
			var level = Resources.Load<GameObject>("LevelEditor/" + levelName);

			if (level == null)
			{
				Debug.LogWarning("Level does not exist!");
				return;
			}
			
			if (LevelHolder)
				Destroy(LevelHolder);
			
			LevelHolder = Instantiate(level, Vector3.zero, Quaternion.identity);
			LevelHolder.name = levelName;
			UpdateCellsContent();
		}

		private void UpdateCellsContent()
		{
			for (var i = 0; i < MAX_LAYERS_COUNT; i++)
			{
				var layer = LevelHolder.transform.GetChild(i);
				foreach (Transform placedObject in layer)
				{
					_gridController.GetCell(placedObject.position).SetPlacedObject(placedObject.GetComponent<PlaceableObjectController>(), i);
				}
			}
		}
	} 
}
