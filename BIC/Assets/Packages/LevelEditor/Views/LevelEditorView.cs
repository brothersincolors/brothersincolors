﻿using Packages.LevelEditor.Controllers;
using Packages.SmartTiling.Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace Packages.LevelEditor.Views
{
	public class LevelEditorView : MonoBehaviour
	{

		public Button IncrementLayerButton;
		public Button DecrementLayerButton;
		public Text LayerValueHolder;

		public Button LoadLevelButton;
		public InputField LevelNameInputField;

		public Button HideGridButton;
		public InputField GridWidthInputField;
		public InputField GridHeightInputField;
		public InputField GridCellSizeInputField;
		public Button RegenerateGridButton;

		private void Start() => AssignUIElementsListeners();

		private void Update()
		{
			LayerValueHolder.text = LevelEditorController.Instance.CurrentLayer.ToString();
		}

		private void AssignUIElementsListeners()
		{
			// Layer
			IncrementLayerButton.onClick.AddListener(LevelEditorController.Instance.IncrementLayer);
			DecrementLayerButton.onClick.AddListener(LevelEditorController.Instance.DecrementLayer);
			
			// Grid
			HideGridButton.onClick.AddListener(LevelEditorController.Instance.HideShowGrid);
			GridWidthInputField.text = LevelEditorController.Instance.WidthInCells.ToString();
			GridHeightInputField.text = LevelEditorController.Instance.HeightInCells.ToString();
			GridCellSizeInputField.text = LevelEditorController.Instance.CellSide.ToString();
			
			RegenerateGridButton.onClick.AddListener(() =>
			{
				LevelEditorController.Instance.RegenerateGrid(
					int.Parse(GridWidthInputField.text),
					int.Parse(GridHeightInputField.text),
					int.Parse(GridCellSizeInputField.text)
					);
				SmartTileManager.Instance.GenerateMatrix(int.Parse(GridWidthInputField.text),
					int.Parse(GridHeightInputField.text), LevelEditorController.MAX_LAYERS_COUNT);
			});
			
			// Loading
			LoadLevelButton.onClick.AddListener(() => LevelEditorController.Instance.LoadLevel(LevelNameInputField.text));
		}
	}
}
