﻿using Packages.LevelEditor.Models;
using UnityEngine;
using UnityEngine.UI;

namespace Packages.LevelEditor.Views
{
	public class TabView : MonoBehaviour
	{

		public Tab[] Tabs;

		public GameObject ActiveTabContent;

		private void Start()
		{
			foreach (var tab in Tabs)
			{
				tab.TabView = this; 
			}
		}

		public void UpdateContentView(GameObject content)
		{
			if (ActiveTabContent)
				ActiveTabContent.SetActive(false);
			ActiveTabContent = content;
			ActiveTabContent.SetActive(true);
		}
	}
}
