﻿using Packages.LevelEditor.Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace Packages.LevelEditor.Views
{
	[RequireComponent(typeof(Button))]
	public class PlacableObjectView : MonoBehaviour
	{
		public PlaceableObjectController PlacableObject;

		private void Start()
		{
			GetComponent<Button> ().onClick.AddListener(() =>
			{
				LevelEditorController.Instance.SetSelectedObject(PlacableObject);
			});
		}
	}
}
