﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ShaksUI
{

    [RequireComponent(typeof(Image))]
    public class UIToggle : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
    {

        public Image targetImage;
        public Color normalColor;
        public Color highlightedColor;

        private UnityEventBool onClick;
        private bool isOn;

        public bool IsOn
        {
            get
            {
                return isOn;
            }
            set
            {
                isOn = value;
            }
        }

        public UnityEventBool OnClick
        {
            get
            {
                return onClick;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            isOn = !isOn;
            onClick.Invoke(isOn);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            targetImage.color = highlightedColor;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (isOn) return;
            targetImage.color = normalColor;
        }

        private void Awake()
        {
            targetImage = GetComponent<Image>();
            if (onClick == null)
                onClick = new UnityEventBool();
        }
    }


    [System.Serializable]
    public class UnityEventBool : UnityEvent<bool>
    {

    }
}