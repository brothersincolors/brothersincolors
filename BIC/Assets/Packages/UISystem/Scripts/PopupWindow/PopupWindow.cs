﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Packages.UISystem.Scripts.PopupWindow
{

    [RequireComponent(typeof(CanvasGroup))]
    public class PopupWindow : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler
    {

        public bool clickOutsideToClose = true;

        private CanvasGroup attCanvasgroup;
        private bool isHidden;
        private bool pointerOver;

        public bool IsHidden
        {
            get
            {
                return isHidden;
            }
        }

        public void FadeIn()
        {
            isHidden = false;
            PopupBackground.popupIsOpened = true;
        }

        public void FadeIn(Action instruction)
        {
            FadeIn();
            instruction();
        }

        public void FadeOut()
        {
            if (!isHidden)
            {
                isHidden = true;
                PopupBackground.popupIsOpened = false;
            }
        }

        public void FadeOut(Action instruction)
        {
            FadeOut();
            instruction();
        }

        public void OnPointerEnter(PointerEventData eventData) { pointerOver = true; }
        public void OnPointerExit(PointerEventData eventData) { pointerOver = false; }

        private void Start()
        {
            attCanvasgroup = GetComponent<CanvasGroup>();
            FadeOut();
        }

        private void Update()
        {
            if (clickOutsideToClose && Input.GetMouseButtonDown(0) && !pointerOver) FadeOut();
        }

        private void FixedUpdate()
        {
            if (isHidden)
            {
                attCanvasgroup.alpha = Mathf.Lerp(attCanvasgroup.alpha, 0, 0.5f);
                attCanvasgroup.interactable = false;
                attCanvasgroup.blocksRaycasts = false;
            }
            else
            {
                attCanvasgroup.alpha = Mathf.Lerp(attCanvasgroup.alpha, 1, 0.5f);
                attCanvasgroup.interactable = true;
                attCanvasgroup.blocksRaycasts = true;
            }
        }
    }
}