﻿using UnityEngine;

namespace Packages.UISystem.Scripts.PopupWindow
{

    [RequireComponent(typeof(CanvasGroup))]
    public class PopupBackground : MonoBehaviour
    {

        public static bool popupIsOpened;

        CanvasGroup attCanvasgroup;

        private void Start()
        {
            attCanvasgroup = GetComponent<CanvasGroup>();
            attCanvasgroup.interactable = false;
        }

        private void Update()
        {
            if (popupIsOpened)
            {
                attCanvasgroup.alpha = Mathf.Lerp(attCanvasgroup.alpha, 0.7f, 0.5f);
                attCanvasgroup.blocksRaycasts = true;
            }
            else
            {
                attCanvasgroup.alpha = Mathf.Lerp(attCanvasgroup.alpha, 0, 0.5f);
                attCanvasgroup.blocksRaycasts = false;
            }
        }
    }
}
