﻿namespace DataBases.KeycodesDataBase
{
    public class KeycodeConverter
    {
        /* Класс, который обрабатывает общий для всех геймпадов keycode, и возвращает уникальный keycode */

        readonly int[] gamepadData;
      
        public KeycodeConverter(string deviceName)      // Конструктор принимает имя геймпада
        {
            if (deviceName != null)
                gamepadData = KeycodeDataBase.Instance.GetGamepadKeycodes(deviceName);      // Вытаскивает из базы данных keycode'ы для геймпада
        }

        public int ConvertKeycode(int keycode)      // Принимает интовое значение GamepadKeycode и отдает keycode кнопки на геймпаде
        {
            if (gamepadData != null)
            {
                return UnityEngine.Mathf.Abs(gamepadData[keycode]);
            }
            return keycode;
        }
    }
}