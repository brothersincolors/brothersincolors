﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

namespace DataBases.KeycodesDataBase
{

    public class KeycodeDataBase : MonoBehaviour
    {
        public UIButtonModel[] UiButtons;
        public bool Log;
        
        private static Dictionary<string, int[]> _supportedGamepads = new Dictionary<string, int[]>();     // Список поддерживаемых геймпадов

        public static KeycodeDataBase Instance { get; private set; }

        public UIButtonModel GetUIModelByName(string name) => UiButtons.First(model => string.Equals(model.Name, name, StringComparison.CurrentCultureIgnoreCase));
 


        // For input control module
        
        public int[] GetGamepadKeycodes(string deviceName)      // Возвращает кнопки геймпада в виде массива, где для кнопки
                                                                // с индексом i соответствует keycode геймпада
        {
            int[] keycodes = null;
            _supportedGamepads?.TryGetValue(deviceName, out keycodes);
            return keycodes;
        }

        public bool AddGamepadData(string deviceName, int[] keycodes)       // Добавляет данные о геймпадах в базу
        {
            if (_supportedGamepads == null)
                _supportedGamepads = new Dictionary<string, int[]>();
            if (!_supportedGamepads.ContainsKey(deviceName))
                _supportedGamepads.Add(deviceName, keycodes);
            else
            {
                _supportedGamepads.Remove(deviceName);
                _supportedGamepads.Add(deviceName, keycodes);
            }
            SaveGamepadData();
            return true;
        }

        public static GamepadKeycode ConvertKey(string deviceName, int keycode, bool isAxis)    // Вытаксивает из базы данных keycode геймпада
        {
            if (_supportedGamepads.ContainsKey(deviceName))
            {
                int[] gamepadData;
                _supportedGamepads.TryGetValue(deviceName, out gamepadData);
                for (int i = 0; i < gamepadData.Length; i++)
                {
                    if (gamepadData[i] == keycode)
                    {
                        return (GamepadKeycode)Enum.GetValues(typeof(GamepadKeycode)).GetValue(i);   
                    }
                }
            }
            return GamepadKeycode.None;
        }

        public static bool IsAxis(GamepadKeycode keycode)       // Проверка keycode на то, что он ось
        {
            if ((int)keycode > 15) return true;
            return false;
        }

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            if (Log)
                if (LoadGamepadsData())
                    print("## KEYCODE DATA BASE ## Gamepads data is loaded.");
                else
                    print("## KEYCODE DATA BASE ## Gamepads data is missing.");
        }

        private bool LoadGamepadsData()     // Загрузка данных о геймпадах
        {
            if (!File.Exists(Application.persistentDataPath + "/gamepads.dat")) return false;
            var bf = new BinaryFormatter();
            var fs = new FileStream(Application.persistentDataPath + "/gamepads.dat", FileMode.Open);
            _supportedGamepads = ((GamepadsData)bf.Deserialize(fs)).data;
            fs.Close();
            return true;
        }

        private bool SaveGamepadData()      // Сохранение данных о геймпадах
        {
            var bf = new BinaryFormatter();
            var fs = new FileStream(Application.persistentDataPath + "/gamepads.dat", FileMode.Create);
            var data = new GamepadsData {data = _supportedGamepads};
            bf.Serialize(fs, data);
            fs.Close();
            return true;
        }

        [Serializable]
        private class GamepadsData
        {
            public Dictionary<string, int[]> data;
        }
    }

    public enum GamepadKeycode
    {
        Action1 = 0, Action2 = 1, Action3 = 2, Action4 = 3, UpperKey = 4, RightKey = 5, DownKey = 6, LeftKey = 7,
        R1 = 8, R2 = 9, L1 = 10, L2 = 11, Select = 12, Start = 13, LeftStickKey = 14, RightStickKey = 15, LeftStickX = 16, 
        LeftStickY = 17, RightStickX = 18, RightStickY = 19, _4thAxis = 20, _5thAxis = 21, _6thAxis = 22, _7thAxis = 23, 
        _8thAxis = 24, _9thAxis = 25, _10thAxis = 26, _11thAxis = 27, _12thAxis = 28, _13thAxis = 29, _14thAxis = 30, _15thAxis = 31,
        _16thAxis = 32, _17thAxis = 33, _18thAxis = 34, _19thAxis = 35, None
    };
}
