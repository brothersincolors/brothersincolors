﻿using System;
using UnityEngine;

namespace DataBases.KeycodesDataBase
{
    [Serializable]
    public class UIButtonModel
    {
        public string Name;
        public Sprite ButtonSprite;
    }
}