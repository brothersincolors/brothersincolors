﻿using System;
using Configs.Skins;
using UnityEngine;
using SkinQuality = Configs.Skins.SkinQuality;

namespace DataBases.SkinDataBase
{
	[Serializable]
	public class SkinModel
	{
		public string Name;
		public SkinQuality Quality;
		public SkinColor Color;
		public RuntimeAnimatorController RuntimeAnimatorController;
		public bool IsOpened;
	}
}
