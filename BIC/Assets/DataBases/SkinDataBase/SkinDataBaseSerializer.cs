﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.Linq;

namespace DataBases.SkinDataBase
{
	public static class SkinDataBaseSerializer
	{

		private const string SkinsStorageName = "/sk_str.dat";
		private const string SkinsStorageFolder = "/skins";
		
		public static void SerializeSkins(SkinModel[] skins)
		{
			var skinSerializationModel = new SkinSerializationModel(skins);
			if (!Directory.Exists(Application.persistentDataPath + SkinsStorageFolder))
			{
				Directory.CreateDirectory(Application.persistentDataPath + SkinsStorageFolder);
			}

			var fileStream = File.Open(Application.persistentDataPath + SkinsStorageFolder + SkinsStorageName,
				FileMode.OpenOrCreate);
			var binaryFormatter = new BinaryFormatter();
			binaryFormatter.Serialize(fileStream, skinSerializationModel);
			fileStream.Close();
		}
		
		public static bool DeserializeSkins(ref SkinModel[] playerSkins)
		{
			if (!File.Exists(Application.persistentDataPath + SkinsStorageFolder + SkinsStorageName)) return false;
			var fileStream = File.Open(Application.persistentDataPath + SkinsStorageFolder + SkinsStorageName,
				FileMode.Open);
			var binaryFormatter = new BinaryFormatter();
			var deserializedSkinModel = (SkinSerializationModel) binaryFormatter.Deserialize(fileStream);
			foreach (var skinData in deserializedSkinModel.SkinsData)
				playerSkins.First(skin => skin.Name == skinData.Key).IsOpened = skinData.Value;
			return true;
		}
	}
}
