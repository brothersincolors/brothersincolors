﻿using System;
using System.Collections.Generic;

namespace DataBases.SkinDataBase
{
	[Serializable]
	public class SkinSerializationModel
	{
		public Dictionary<string, bool> SkinsData;

		public SkinSerializationModel(SkinModel[] skins)
		{
			SkinsData = new Dictionary<string, bool>();
			foreach (var skin in skins)
			{
				SkinsData.Add(skin.Name, skin.IsOpened);
			}
		}
	}
}
