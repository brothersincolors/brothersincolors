﻿using System.Linq;
using UnityEngine;
using Configs;
using Configs.Skins;

namespace DataBases.SkinDataBase
{
	public class SkinDataBase : MonoBehaviour
	{
		public static SkinDataBase Instance { get; private set; }
		
		public SkinModel[] PlayerSkins;

		public SkinModel GetPlayerSkin(string skinName)
		{
			var result = PlayerSkins.First(skin => skin.Name == skinName);
			return result;
		}

		public SkinModel[] GetPlayerSkins(DefaultPlayerColor playerColor)
		{
			return PlayerSkins.Where(skin => (int) skin.Color == (int) playerColor || skin.Color == SkinColor.None).ToArray();
		}

		private void Awake()
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}

		private void Start()
		{
			SkinDataBaseSerializer.DeserializeSkins(ref PlayerSkins);
		}
	}
}
