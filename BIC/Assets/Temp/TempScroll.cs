﻿using UnityEngine;

namespace Temp
{
	public class TempScroll : MonoBehaviour
	{
		public float Speed = 0.1f;
		
		private Material _material;
		
		private void Start ()
		{
			_material = GetComponent<Renderer>().material;
		}
	
		private void Update () 
		{
			_material.mainTextureOffset += new Vector2(Speed, 0) * Time.deltaTime;
		}
	}
}
