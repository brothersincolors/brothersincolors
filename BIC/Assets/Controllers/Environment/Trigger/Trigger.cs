﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Controllers.GameControllers;

namespace Controllers.Environment
{
    public class Trigger : MonoBehaviour
    {
        public int
            TriggeredTo =
                0; //На кого из игроков будет реагировать данный триггер;Можно будет запариться и сделать еще и по цвету

        public List<GameObject> TriggeringObject;

        public void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.CompareTag("Player"))
            {
                if (TriggeredTo == 0)
                    CallTriggers(TriggeringObject);
                else if (collider == MainGameplayController.Instance.PlayerControllers[TriggeredTo - 1]
                             .GetComponent<Collider2D>())
                    CallTriggers(TriggeringObject);
            }

        }

        private void CallTriggers(List<GameObject> triggeringObject)
        {
            foreach (var obj in triggeringObject)
            {
                obj.GetComponent<ITriggerable>().Action();
            }
        }
    }
}
