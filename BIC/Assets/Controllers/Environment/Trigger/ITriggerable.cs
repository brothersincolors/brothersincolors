﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controllers.Environment
{
    public interface ITriggerable
    {
        void Action();
    }
}

