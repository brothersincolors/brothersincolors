﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controllers.Environment
{
    public class DestroyTriggerable : MonoBehaviour, ITriggerable
    {
        public float Time = 1;

        public void Action()
        {
            Invoke("DestroySelf", Time);
        }

        private void DestroySelf() => Destroy(gameObject);
    }
}