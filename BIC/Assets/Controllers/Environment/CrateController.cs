﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Preferences;

namespace Controllers.Environment
{
    public class CrateController : MonoBehaviour
    {
        public ParticleSystem ParticleSys;
        public int Portion = 1;

        private void OnTriggerEnter2D(Collider2D obj)
        {
            if (obj.CompareTag("Player"))
            {
                ParticleSys.Emit(100);
                Prefs.Tokens += Portion;
                Invoke("DestroySelf", 2f);
                GetComponent<SpriteRenderer>().enabled = false;
            }
        }
        private void DestroySelf()
        {
            Destroy(gameObject);
        }
    }

}