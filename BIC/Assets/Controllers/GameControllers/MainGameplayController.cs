﻿using System.Collections.Generic;
using Configs.Player;
using Controllers.Cameras;
using Controllers.Effects;
using Controllers.Player;
using Events;
using UnityEngine;

namespace Controllers.GameControllers
{
    [RequireComponent(typeof(GameplayCameraController))]
    public class MainGameplayController : MonoBehaviour
    {
        [Header("\tGame controll in level")]
        public GameObject PlayerPrefab;
        public PlayerColor[] PlayerColors = new PlayerColor[7];
        public Dictionary<string, Color> Colors;

        public static Vector3
            LastCheckpointPosition = Vector3.zero;

        public static bool White;

        public PlayerController[] PlayerControllers { get; } = new PlayerController[3];

        public ColorChanger[] PlayerChangers;

        [HideInInspector] public GameObject[] Players;

        static MainGameplayController _instance;

        private Collider2D[] _playerColliders;

        private const int PlayersAmount = 3;

        public static MainGameplayController Instance => _instance;

        private void Awake()
        {
            _instance = this;
            Colors = new Dictionary<string, Color>();
            foreach (var i in PlayerColors)
                Colors.Add(i.Name, i.Color);
        }

        private void Start()
        {
            SpawnPlayers();
        }

        private void SpawnPlayers()
        {
            _playerColliders = new Collider2D[PlayersAmount];
            Players = new GameObject[PlayersAmount];
            PlayerChangers = new ColorChanger[PlayersAmount];
            var playerTransforms = new Transform[PlayersAmount];


            for (var i = 0; i < 3; i++)
            {
                Players[i] = Instantiate(PlayerPrefab, Vector3.zero, Quaternion.identity);
                PlayerControllers[i] = Players[i].GetComponent<PlayerController>();
                PlayerControllers[i].Color = PlayerColors[i].Color;
                PlayerControllers[i].Index = i;
                _playerColliders[i] = Players[i].GetComponent<Collider2D>();
                PlayerChangers[i] = Players[i].transform.GetChild(0).GetComponent<ColorChanger>();
                PlayerChangers[i].Col = PlayerColors[i];
                playerTransforms[i] = Players[i].transform;
            }

            for (var i = 0; i < 3; i++)
            {
                var k = 0;
                for (var j = 0; j < 3; j++)
                {
                    if (i != j)
                        PlayerChangers[i].OtherColliders[k++] = PlayerChangers[j].GetComponent<Collider2D>();
                }
            }

            EventAggregator.PlayersSpawned();
        }

        private void DeactivatePlayers()
        {
            for (var i = 0; i < 3; i++)
                Players[i].SetActive(false);

            EventAggregator.PlayerDeath();
        }

        private void ActivatePlayers()
        {
            for (var i = 0; i < 3; i++)
            {
                Players[i].transform.position = LastCheckpointPosition;
                Players[i].SetActive(true);
            }

            GameStateMachine.CurrentPlayersState = GameStateMachine.PlayersState.Alive;
        }


        public void RestartFromCheckpoint(PlayerController player)
        {
            PlayerDeathEffectController.Instance.EmitBlood(player.transform.position, player.CurrentColor);
            player.gameObject.SetActive(false);
            if (GameStateMachine.CurrentPlayersState == GameStateMachine.PlayersState.Dead) return;
            GameStateMachine.CurrentPlayersState = GameStateMachine.PlayersState.Dead;
            Invoke("RestartFromCheckpoint", 0.3f);
        }

        public void RestartFromCheckpoint()
        {
            if(SwapColorController.Instance!=null) SwapColorController.Instance.DefaultColors();
            DeactivatePlayers();
            ActivatePlayers();
        }
    }
}