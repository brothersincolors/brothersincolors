﻿using Events;
using UnityEngine;

namespace Controllers.GameControllers
{
	public class TrippleModeController : MonoBehaviour {
		private void Awake()
		{
			if (GameStateMachine.CurrentGameMode != GameStateMachine.GameMode.Tripple)
			{
				enabled = false;
				return;
			}
			print("Tripple mode.");
			EventAggregator.SubscribeOnPlayersSpawn(OnPlayersSpawned);
		}

		private void OnPlayersSpawned()
		{
			for (var i = 0; i <  MainGameplayController.Instance.PlayerControllers.Length; i++)
			{
				MainGameplayController.Instance.PlayerControllers[i].SetInputDevice(
					Preferences.Prefs.PlayerInputDevices[i],
					Preferences.Prefs.PlayerKeyboardControls[i]
				);
			}
		}

		private void OnDestroy()
		{
			EventAggregator.UnsubscribeFromPlayersSpawn(OnPlayersSpawned);
		}
	}
}
