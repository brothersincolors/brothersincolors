﻿using System.Runtime.Serialization.Formatters;
using Configs.Player;
using Controllers.Player;
using DataBases.KeycodesDataBase;
using Events;
using InputControl;
using UnityEngine;
using Views.GameMode;

namespace Controllers.GameControllers
{
	public class SoloModeController : MonoBehaviour
	{
		public SoloGameModeView SoloGameModeView;
		
		private PlayerKeyboardControls _playerKeyboardControls;
		private InputDevice _inputDevice;

		private bool _inputDevicesAreAssigned;
		private bool _leftPlayerIsHold;
		private bool _rightPlayerIsHold;
		
		public PlayerController[] PlayerControllers { get; private set; }
		public int ControlledPlayerIndex { get; private set; }

		public int FixPlayerIndex(int playerIndex)
		{
			if (playerIndex < 0)
				return 2;
			if (playerIndex > 2)
				return 0;
			return playerIndex;
		}

		private async void SetInputDevices()
		{
			await new WaitForEndOfFrame();
			PlayerControllers[ControlledPlayerIndex].SetInputDevice(_inputDevice, _playerKeyboardControls);
		}
		
		
		private void Awake()
		{
			if (GameStateMachine.CurrentGameMode != GameStateMachine.GameMode.Solo)
			{
				enabled = false;
				return;
			}
			print("Solo mode");
			EventAggregator.SubscribeOnPlayersSpawn(OnPlayersSpawned);
		}

		private void OnPlayersSpawned()
		{
			_playerKeyboardControls = Preferences.Prefs.PlayerKeyboardControls[0];
			_inputDevice = Preferences.Prefs.PlayerInputDevices[0];

			PlayerControllers = MainGameplayController.Instance.PlayerControllers;
			
			PlayerControllers[0].SetInputDevice(_inputDevice, _playerKeyboardControls);
			_inputDevicesAreAssigned = true;
			
			SoloGameModeView.ActivateTips();
		}

		private void OnDestroy() => EventAggregator.UnsubscribeFromPlayersSpawn(OnPlayersSpawned);

		private void Update()
		{
			if (!_inputDevicesAreAssigned) return;

			if (Input.GetKeyDown(KeyCode.U) || _inputDevice != null && _inputDevice.GetKeyDown(GamepadKeycode.L2))
			{
				if (_leftPlayerIsHold)
					PlayerControllers[FixPlayerIndex(ControlledPlayerIndex - 1)].RemoveInputDevice();
				if (_rightPlayerIsHold)
					PlayerControllers[FixPlayerIndex(ControlledPlayerIndex + 1)].RemoveInputDevice();
				PlayerControllers[ControlledPlayerIndex].RemoveInputDevice();
				ControlledPlayerIndex = FixPlayerIndex(ControlledPlayerIndex - 1);
				SetInputDevices();
				SoloGameModeView.RecolorTips();
			}

			if (Input.GetKeyDown(KeyCode.O) || _inputDevice != null && _inputDevice.GetKeyDown(GamepadKeycode.R2))
			{
				if (_leftPlayerIsHold)
					PlayerControllers[FixPlayerIndex(ControlledPlayerIndex - 1)].RemoveInputDevice();
				if (_rightPlayerIsHold)
					PlayerControllers[FixPlayerIndex(ControlledPlayerIndex + 1)].RemoveInputDevice();
				PlayerControllers[ControlledPlayerIndex].RemoveInputDevice();
				ControlledPlayerIndex = FixPlayerIndex(ControlledPlayerIndex + 1);
				SetInputDevices();
				SoloGameModeView.RecolorTips();
			}

			if (Input.GetKeyDown(KeyCode.Q) || _inputDevice != null && _inputDevice.GetKeyDown(GamepadKeycode.L1))
			{
				PlayerControllers[FixPlayerIndex(ControlledPlayerIndex - 1)]
					.SetInputDevice(_inputDevice, _playerKeyboardControls);
				_leftPlayerIsHold = true;
			}
			
			if (Input.GetKeyUp(KeyCode.Q) || _inputDevice != null && _inputDevice.GetKeyUp(GamepadKeycode.L1))
			{
				PlayerControllers[FixPlayerIndex(ControlledPlayerIndex - 1)].RemoveInputDevice();
				_leftPlayerIsHold = false;
			}
			
			if (Input.GetKeyDown(KeyCode.W) || _inputDevice != null && _inputDevice.GetKeyDown(GamepadKeycode.R1))
			{
				PlayerControllers[FixPlayerIndex(ControlledPlayerIndex + 1)]
					.SetInputDevice(_inputDevice, _playerKeyboardControls);
				_rightPlayerIsHold = true;
			}
			
			if (Input.GetKeyUp(KeyCode.W) || _inputDevice != null && _inputDevice.GetKeyUp(GamepadKeycode.R1))
			{
				PlayerControllers[FixPlayerIndex(ControlledPlayerIndex + 1)].RemoveInputDevice();
				_rightPlayerIsHold = false;
			}

		}
		
	}
}
