﻿using Controllers.SaveLoadManagement;
using UnityEngine;
using UnityEngine.UI;

namespace Controllers.Settings
{
    public class SettingsManager : MonoBehaviour {

        /* Код, который отвечает за настройки.
     */

        [Header("UI Video settings")]
        public Dropdown ResolutionDropdown;     // Dropdown для разрешения экрана
        public Toggle FullscreenToggle;     // Toggle для фуллскрина
        public Dropdown VsyncDropdown;      // Dropdown для VSync

        static SettingsManager _instance;        // Ссылка на себя

        public static SettingsManager Instance { get { return _instance; }}

        VideoSettingsData _videoSettingsData = new VideoSettingsData();  // Настройки видео

        private void Awake()
        {
            _instance = this;
        }

        void Start()
        {
            // Засовываем все разрешения нашего экрана в Dropdown
            var resolutions = Screen.resolutions;
            int k = 0;
            foreach (Resolution item in resolutions)
            {
                ResolutionDropdown.options.Add( new Dropdown.OptionData(SerializableResolution.ResolutionToString(item)));
                if (item.Equals(Screen.currentResolution)) ResolutionDropdown.value = k;
                k++;
            }

            // Пытаемся подгрузить настройки видео
            if (SaveLoadManagementController.Load(ref _videoSettingsData))
            {
                // Ел=сли они есть, то засовываем их во все dropdown и toggle
                Screen.SetResolution(_videoSettingsData.Resolution.Width, _videoSettingsData.Resolution.Height, _videoSettingsData.FullScreen);
                QualitySettings.vSyncCount = _videoSettingsData.VSync;
                FullscreenToggle.isOn = _videoSettingsData.FullScreen;
                VsyncDropdown.value = _videoSettingsData.VSync;
                ResolutionDropdown.value = ResolutionDropdown.options.FindIndex((Dropdown.OptionData data) => (data.text == _videoSettingsData.Resolution.ToString()));
            }
            // Накидиываем методы, которые будут вызываться при нажатиях на наши dropdown и toggle
            ResolutionDropdown.onValueChanged.AddListener(delegate
            {
                Screen.SetResolution(resolutions[ResolutionDropdown.value].width, resolutions[ResolutionDropdown.value].height, Screen.fullScreen);
                var resolution = resolutions[ResolutionDropdown.value];
                _videoSettingsData.Resolution = new SerializableResolution(resolution.width, resolution.height);
                SaveLoadManagementController.Save(_videoSettingsData);
            });
            FullscreenToggle.onValueChanged.AddListener(delegate {
                Screen.fullScreen = FullscreenToggle.isOn;
                _videoSettingsData.FullScreen = FullscreenToggle.isOn;
                SaveLoadManagementController.Save(_videoSettingsData);
            });
            VsyncDropdown.onValueChanged.AddListener(delegate {
                QualitySettings.vSyncCount = VsyncDropdown.value;
                _videoSettingsData.VSync = VsyncDropdown.value;
                SaveLoadManagementController.Save(_videoSettingsData);
            });
        }
    }
}
