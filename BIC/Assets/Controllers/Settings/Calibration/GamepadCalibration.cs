﻿using DataBases.KeycodesDataBase;
using InputControl;
using UnityEngine;
using UnityEngine.UI;

namespace Controllers.Settings.Calibration
{
    public class GamepadCalibration : MonoBehaviour {
    
        public Text Highlight;      // Заголовок
        public Text Tip;            // Подсказка на что нажать

        private GameObject[] _childs;        // Каждая кнопка
        private InputControl.DeviceClass.Device _device, _previousDevice;     // Дабы инпут не поступал с разных устройств
        // запоминаем устройство с которого идет инпут, 
        // и устройство с которого идет инпут сейчас
        private bool _calibrate = false;     // Калибруем ли мы сейчас
        private int _i = 0;      // Индекс кнопки, которыую калибруем сейчас
        private int[] _gamepadData = new int[20];        // То где мы храним кейкоды кнопок
        private bool[] _keysIsAssigned = new bool[20];   // Для каждой кнопки(оси) правдива, если мы ее уже откалибровали
        private int _previousButton = int.MinValue;      // Индекс предыдущей кнопки

        private void Start()
        {
            // Вытаскиваем все кнопки
            _childs = new GameObject[transform.childCount];     
            int k = 0;
            foreach (Transform child in transform)
                _childs[k++] = child.gameObject;
            // Подсвечиваем заголовок
            Highlight.gameObject.SetActive(false);
        }

        public void OnReset()       // Вызывается для калиброки еще одного геймпада
        {
            // Обновляет все данные
            _calibrate = false;
            _i = 0;
            _gamepadData = new int[20];
            _keysIsAssigned = new bool[20];
            _previousButton = int.MinValue;
            Highlight.gameObject.SetActive(false);
            Tip.gameObject.SetActive(true);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space) && !_calibrate)      // Кнопка начла калибровки
            {
                _calibrate = true;
                _gamepadData = new int[36];
                _keysIsAssigned = new bool[36];
                _device = null;
                _previousDevice = null;
                _i = 0;
                _previousButton = int.MinValue;
                _previousDevice = null;
                Tip.gameObject.SetActive(false);        // Вырубаем подсказкц
                Highlight.gameObject.SetActive(false);  // Вырубаем заголовок
            }


            if (_calibrate)      // Если идет калибровка
            {
                if (_device != null)     // И у нас уже есть геймпад, который мы калибруем
                {
                    _device.isCalibrating = true;        // Помечаем девайс, как калибрующийся. С него не будет читаться инпут
                }
                if (_i == 20)        // Если мы закончили калибровку всех 20 кнопок, то сохраняем данные и выходим из калибровки
                {
                    _childs[_i - 1].SetActive(false);     // Деативируем последнюю кнопку
                    _previousDevice.isCalibrating = false;       // Девайс больше не калибрутеся
                    KeycodeDataBase.Instance.AddGamepadData(_previousDevice.Name, _gamepadData);  // Добавляем в базу данных о 
                    // геймпадах данные о нашем геймпаде
                    _calibrate = false;      // Заканчиваем калибровку
                    Highlight.gameObject.SetActive(true);       // Подсвечиваем заголовок
                    return;
                }
                if (_keysIsAssigned[_i])      // Если кнопка уже откалибрована идем дальше (Такое возможно когда стрелки на геймпаде не кнопки, а оси)
                {
                    _i++;
                    return;
                }
                _childs[_i].SetActive(true);      // Активируем кнопку в UI
                int button = 0;     
                // Если это 100% не кнопка, и мы двигаем какую-то ось это ось, в противном случае считываем кнопку
                // В геймпаде DS4, R2, L2 являются одновременно и кнопками и осями, потому важно проверить, точно ли это сосок.
                if (((_i < 8 || _i > 14) && InputManager.GlobalJoystick.GetJoystickAxis(out button, out _device)) || InputManager.GlobalJoystick.GetJoystickButtonDown(out button, out _device))
                {
                    if (_previousDevice == null)     // Если мы еще не запомнили девайс с которго начали калибровку, то запоминаем его
                        _previousDevice = _device;
                
                    if (_previousDevice != _device)   // Если нажали кнопку с левого девайса, то игнорим ее.
                    {
                        return;
                    }

                    if (_previousButton == button)   // Если жмякаем на ту же самую кнопку, то игнорим ее
                        return;
                    _previousButton = button;

                    _childs[_i].SetActive(false);     // Деактивим кнопку в UI 
                    _keysIsAssigned[_i] = true;       // Говорим, что кнопка откалибрована
                    _gamepadData[_i++] = button;      // Записываем ее в данные геймпада и идем в след кнопке
                }
            }
        }
    }
}
