﻿using System.Collections.Generic;
using Controllers.Cameras;
using Controllers.GameControllers;
using Extensions.Classes;
using UnityEngine;

namespace Controllers.Sky
{
    [RequireComponent(typeof(Pooler))]
    public class SkyManager : MonoBehaviour
    {
        public bool Pool = false;

        public PoolInstruction[] PoolingObjects;
        public Transform[] Parallaxes;
        public float YOffset = 2f;

        private Pooler _pooler;
        private Dictionary<float, GameObject> _skyObjects = new Dictionary<float, GameObject>();

        private Vector3 _screenCenterOffset;

        private static SkyManager _instance;

        public static SkyManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public void OnSkyObjcetBecameInvisible(SkyObject obj)
        {
            if (!_skyObjects.ContainsKey(obj.EndSleepTime))
                _skyObjects.Add(obj.EndSleepTime, obj.gameObject);
            else
                _skyObjects.Add(obj.EndSleepTime + 0.02f, obj.gameObject);
            obj.gameObject.SetActive(false);
        }

        private void Awake()
        {
            if (_instance != null)
                DestroyImmediate(_instance.gameObject);
            _instance = this;
        }

        private void Start()
        {
            var camera = GameplayCameraController.Instance.GetCamera();
            _screenCenterOffset = (camera.ScreenToWorldPoint(new Vector3(camera.pixelWidth, camera.pixelHeight)) - camera.ScreenToWorldPoint(new Vector3(0, 0))) / 2f + Vector3.right;

            if (!Pool) return;
            _pooler = GetComponent<Pooler>();
            for (int i = 0; i < PoolingObjects.Length; i++)
            {
                var pooledObjects = _pooler.Initialize(PoolingObjects[i].Prefab.gameObject, PoolingObjects[i].Count);
                int parallaxIndex = 0;
                int incrementer = PoolingObjects[i].Count / Parallaxes.Length + 1;
                int index = 0;
                foreach (var obj in pooledObjects)
                {
                    obj.transform.SetParent(Parallaxes[parallaxIndex]);
                    obj.GetComponent<SpriteRenderer>().sortingLayerName = Parallaxes[parallaxIndex].name;
                    index++;
                    if (index % incrementer == 0) parallaxIndex++;
                }
            }

            var list = _pooler.GetPoolingObjects();
            foreach (var item in list)
            {
                var DeltaGeneration = item.GetComponent<SpriteRenderer>().bounds.size;
                DeltaGeneration.x += 0.1f + Random.Range(0, 1f);
                var fixedPosition = MainGameplayController.Instance.transform.position + (Vector3.right * _screenCenterOffset.x + DeltaGeneration / 2) * Mathf.Sign(Random.Range(-1, 1));
                fixedPosition.y = MainGameplayController.Instance.transform.position.y + Random.Range(_screenCenterOffset.y * 0.25f, _screenCenterOffset.y);
                item.transform.position = fixedPosition;
                item.SetActive(true);
            }
        }

        private void Update()
        {
            List<float> keysToDelete = new List<float>();
            foreach (var pair in _skyObjects)
            {
                if (pair.Key <= Time.time)
                {
                    keysToDelete.Add(pair.Key); ;
                    var DeltaGeneration = pair.Value.GetComponent<SpriteRenderer>().bounds.size;
                    DeltaGeneration.x += 0.1f + Random.Range(0, 1f);
                    var fixedPosition = MainGameplayController.Instance.transform.position + (Vector3.right * _screenCenterOffset.x + DeltaGeneration / 2) * Mathf.Sign(Random.Range(-1, 1));
                    fixedPosition.y = MainGameplayController.Instance.transform.position.y + Random.Range(_screenCenterOffset.y * 0.25f, _screenCenterOffset.y);
                    pair.Value.transform.position = fixedPosition;
                    pair.Value.SetActive(true);
                }
            }
            foreach (var key in keysToDelete)
            {
                _skyObjects.Remove(key);
            }
        }
    }
}
