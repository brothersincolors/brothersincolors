﻿using Controllers.GameControllers;
using Controllers.Cameras;
using UnityEngine;

namespace Controllers.Sky
{

    public class SkyObject : MonoBehaviour
    {

        public float Speed = 1;
        public float SleepTime = 3;
        public float RandomSleepDelta = 0.8f;
        public bool FlipOrientation = false;
        public bool NecessaryToSee = true;

        [HideInInspector]
        public float EndSleepTime = 0;

        private SpriteRenderer _attRenderer;
        private Vector3 _direction = Vector3.right;
        private ScreenOrigin _origins;

        private void OnEnable()
        {
            _direction = MainGameplayController.Instance.transform.position - transform.position;
            _direction.y = 0;
            _direction.z = 0;
            Speed += Random.RandomRange(-.2f, .2f);
            if (_attRenderer == null)
                _attRenderer = GetComponent<SpriteRenderer>();
            if (FlipOrientation)
                _attRenderer.flipX = _direction.x > 0;
        }

        private void Start()
        {
            RandomSleepDelta = Mathf.Abs(RandomSleepDelta);
            _attRenderer = GetComponent<SpriteRenderer>();
            _direction = MainGameplayController.Instance.transform.position - transform.position;
            _direction.y = 0;
            _direction.z = 0;
            var camera = MainGameplayController.Instance.GetComponent<Camera>();
            var screenSize = (camera.ScreenToWorldPoint(new Vector3(camera.pixelWidth, camera.pixelHeight)) - camera.ScreenToWorldPoint(new Vector3(0, 0))) + new Vector3(3, 0, 0) + _attRenderer.bounds.size*1.5f;
            _origins = new ScreenOrigin(MainGameplayController.Instance.transform, screenSize);
        }

        private void Update()
        {
            transform.Translate(_direction.normalized * Speed * Time.deltaTime);

            if (NecessaryToSee)
            {
                if (!_origins.Contains(transform.position))
                {
                    EndSleepTime = Time.time + SleepTime + Random.Range(-RandomSleepDelta, RandomSleepDelta);
                    SkyManager.Instance.OnSkyObjcetBecameInvisible(this);
                }
            }
            else
            {
                _origins.RecalculateOrigins();
                if (!_origins.Contains(transform.position))
                    SkyManager.Instance.OnSkyObjcetBecameInvisible(this);
            }
        }
    }

}