﻿using System.Collections.Generic;
using System.Threading;
using Controllers.LevelManagement;
using DataBases.KeycodesDataBase;
using InputControl;
using InputControl.DeviceClass;
using UnityEngine;

namespace Controllers.GamepadCalibration
{
	public class GamepadCalibrationController : MonoBehaviour
	{

		private bool _isCalibrating;

		private int[] _gamepadData;
		private Device _calibratingDevice;

		private int _calibrationIndex;
		
		private List<GamepadKeycode> _gamepadKeycodesToCalibrate = new List<GamepadKeycode>();

		public void StartCalibration()
		{
			_isCalibrating = true;
			_gamepadData = new int[36];
			_calibrationIndex = 0;
			_calibratingDevice = null;
		}

		public void StopCalibration() => _isCalibrating = false;

		public bool CalibrateGamepadKeycode(GamepadKeycode keycode)
		{
			Device temporaryDeviceHolder = null;
			int temporaryKeycodeDataHolder = 0;

			if (!KeycodeDataBase.IsAxis(keycode))
			{
				if (!InputManager.GlobalJoystick.GetJoystickButtonDown(out temporaryKeycodeDataHolder,
					out temporaryDeviceHolder))
					return false;
				
			}
			else
			{
				if (!InputManager.GlobalJoystick.GetJoystickAxis(out temporaryKeycodeDataHolder,
					out temporaryDeviceHolder))
					return false;
			}
			
			print("## STUFF ##");
			
			if (_calibratingDevice == null)
				_calibratingDevice = temporaryDeviceHolder;
			else if (!_calibratingDevice.EqualsTo(temporaryDeviceHolder))
			{
				print("## DIFF DEVICES");
				return false;
			}

			_gamepadData[(int) keycode] = temporaryKeycodeDataHolder;
			return true;
		}

		private void Start()
		{
			_gamepadKeycodesToCalibrate.Add(GamepadKeycode.Start);
			_gamepadKeycodesToCalibrate.Add(GamepadKeycode.Action2);
			_gamepadKeycodesToCalibrate.Add(GamepadKeycode.Action3);
			_gamepadKeycodesToCalibrate.Add(GamepadKeycode.Action4);
			_gamepadKeycodesToCalibrate.Add(GamepadKeycode.R1);
			_gamepadKeycodesToCalibrate.Add(GamepadKeycode.L1);
			_gamepadKeycodesToCalibrate.Add(GamepadKeycode.R2);
			_gamepadKeycodesToCalibrate.Add(GamepadKeycode.L2);
			_gamepadKeycodesToCalibrate.Add(GamepadKeycode.LeftStickX);
			
			StartCalibration();
		}

		private async void Update()
		{
			if (!_isCalibrating) return;
			
			if (CalibrateGamepadKeycode(_gamepadKeycodesToCalibrate[_calibrationIndex]))
			{
				_calibrationIndex++;
				print("KEY CALIBRATED SUCCESFULLY");
				if (_calibrationIndex == _gamepadKeycodesToCalibrate.Count)
				{
					StopCalibration();
					KeycodeDataBase.Instance.AddGamepadData(_calibratingDevice.Name, _gamepadData);
					await SceneManagmentController.Instance.LoadSceneByName("scene_ui_mainmenu");
				}
			}
			else
			{
				print(_gamepadKeycodesToCalibrate[_calibrationIndex]);
			}
		}
	}
}
