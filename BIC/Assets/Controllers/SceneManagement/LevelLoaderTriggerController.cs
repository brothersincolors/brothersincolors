﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using Controllers.Player;
using UnityEngine;
using Views.LevelLoading;
public class LevelLoaderTriggerController : MonoBehaviour
{
    public string SceneName = "";
    public float prepareTime = 0;

    private bool _isLoading = false;
    
    public void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.CompareTag("Player"))
        {
            if (collider.gameObject.GetComponent<SpriteRenderer>().color == Color.white)
            {
                if (_isLoading) return;

                _isLoading = true;
                gameObject.GetComponent<Collider2D>().isTrigger = false;
                LevelLoadingView.Instance.LoadSceneByName(SceneName);
            }
        }

    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.L) && Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (_isLoading) return;

            _isLoading = true;
            LevelLoadingView.Instance.LoadSceneByName(SceneName);
        }
    }

}