﻿using System;
using System.Collections;
using System.Threading.Tasks;
using Controllers.LoadingScreen;
using Controllers.SaveLoadManagement;
using Events;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Controllers.LevelManagement
{
	public class SceneManagmentController : MonoBehaviour
	{
		/* Контролирует все загрузки */

		public delegate void OnSceneLoaded();
		public static event OnSceneLoaded SceneLoaded;

		private static GameState _currentGameState = GameState.MENU;
		private static int _currentSceneIndex = 0;

		public static SceneManagmentController Instance { get; private set; }


		public static int CurrentSceneIndex
		{
			get { return _currentSceneIndex; }
		}

		public static GameState CurrentGameState
		{
			get { return _currentGameState; }
		}

		private void Awake()
		{
			DontDestroyOnLoad(gameObject);
			_currentSceneIndex = 0;
			Instance = this;
		}

		public static void SetGamestate(GameState state)
		{
			_currentGameState = state;
		}

		public IEnumerator StartNewGame()       // Начинаем новую игру
		{
			var loadingScene = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
			_currentSceneIndex = 1;
			SaveLoadManagementController.Save(_currentSceneIndex);
			while (!loadingScene.isDone)
				yield return new WaitForSeconds(0.1f);
			SceneManager.SetActiveScene(SceneManager.GetSceneAt(_currentSceneIndex));
			_currentGameState = GameState.RUNNING;
			if (SceneLoaded != null)
				SceneLoaded();

		}

		public IEnumerator StartNewGame(Action instruction)
		{
			StartCoroutine(StartNewGame());
			instruction();
			yield return null;
		}

		public IEnumerator ContinueGame()   // Продолжаем игру
		{
			var loadingScene = SceneManager.LoadSceneAsync(_currentSceneIndex, LoadSceneMode.Additive);
			while (!loadingScene.isDone)
				yield return new WaitForSeconds(0.1f);
			SceneManager.SetActiveScene(SceneManager.GetSceneAt(_currentSceneIndex));
			_currentGameState = GameState.RUNNING;
			if (SceneLoaded != null)
				SceneLoaded();

		}

		public IEnumerator ContinueGame(Action instruction)
		{
			StartCoroutine(ContinueGame());
			instruction();
			yield return null;
		}

		public IEnumerator LoadNextLevel()      // Загружаем следующий уровень
		{
			if (_currentSceneIndex + 1 >= SceneManager.sceneCountInBuildSettings) yield break;
			var loadingScene = SceneManager.LoadSceneAsync(_currentSceneIndex + 1, LoadSceneMode.Additive);
			while (!loadingScene.isDone)
				yield return new WaitForSeconds(0.1f);
			SceneManager.SetActiveScene(SceneManager.GetSceneAt(_currentSceneIndex + 1));
			var unloadingScene = SceneManager.UnloadSceneAsync(_currentSceneIndex);
			while (!unloadingScene.isDone)
				yield return new WaitForSeconds(0.1f);
			_currentGameState = GameState.RUNNING;
			if (SceneLoaded != null)
				SceneLoaded();
			EventAggregator.LevelLoaded();
		}

		public IEnumerator LoadSceneAtIndex(int index)      // Загружаем какую то особенную сцену
		{
			if (!(index < 1 && index >= SceneManager.sceneCount)) yield return null;
			var loadingScene = SceneManager.LoadSceneAsync(index, LoadSceneMode.Additive);
			_currentSceneIndex = index;
			SaveLoadManagementController.Save(_currentSceneIndex);
			while (!loadingScene.isDone)
				yield return new WaitForSeconds(0.1f);
			SceneManager.SetActiveScene(SceneManager.GetSceneAt(_currentSceneIndex));
			_currentGameState = GameState.RUNNING;
			if (SceneLoaded != null)
				SceneLoaded();

		}

		public IEnumerator LoadSceneAtIndex(int index, Action instruction)
		{
			StartCoroutine(LoadSceneAtIndex(index));
			instruction();
			yield return null;
		}


		public async Task LoadSceneByName(string sceneName, float prepareTime = 0)
		{
			await LoadingScreenController.Instance.FadeIn();
			await SceneManager.LoadSceneAsync(sceneName);
			await new WaitForSeconds(prepareTime);
			await LoadingScreenController.Instance.FadeOut();
		}

		public IEnumerator RestartCurrentLevel()        // Перезагрузка текущего уровня
		{
			var unloadingScene = SceneManager.UnloadSceneAsync(_currentSceneIndex);
			while (!unloadingScene.isDone)
				yield return new WaitForSeconds(0.1f);
			var loadingScene = SceneManager.LoadSceneAsync(_currentSceneIndex, LoadSceneMode.Additive);
			while (!loadingScene.isDone)
				yield return new WaitForSeconds(0.1f);
			SceneManager.SetActiveScene(SceneManager.GetSceneAt(_currentSceneIndex));
			_currentGameState = GameState.RUNNING;
			if (SceneLoaded != null)
				SceneLoaded();
		}

		public IEnumerator ExitToMainMenu()
		{
			_currentGameState = GameState.MENU;
			var unloadingScene = SceneManager.UnloadSceneAsync(_currentSceneIndex);
			while (!unloadingScene.isDone)
				yield return new WaitForSeconds(0.1f);
			SceneManager.SetActiveScene(SceneManager.GetSceneAt(0));
			if (SceneLoaded != null)
				SceneLoaded();
		}

		public IEnumerator ExitToMainMenu(Action instruction)
		{
			StartCoroutine(ExitToMainMenu());
			instruction();
			yield return null;
		}

		public void ExitToDescktop()
		{
			Application.Quit();
		}

		private async void Update()
		{
			if (Input.GetKeyDown(KeyCode.R) && Input.GetKeyDown(KeyCode.P) && Input.GetKeyDown(KeyCode.LeftShift))
				await LoadSceneByName("scene_ui_mainmenu");
			if (Input.GetKeyDown(KeyCode.Alpha1) && Input.GetKeyDown(KeyCode.LeftShift))
				await LoadSceneByName("CaveBT", 2);
		}
	}

	public enum GameState { RUNNING, PAUSED, MENU, LOADING };
}