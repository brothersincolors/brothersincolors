﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Threading.Tasks;
using Events;

namespace Controllers.LoadingScreen
{
	public class LoadingScreenController : MonoBehaviour
	{

		public CanvasGroup LoadingScreen;
		
		[Header("SETTINGS")] 
		public float FadeInOutTime;
		public float FadeInOutFrames;

		private float _fadeInOutTimeDelta;
		private float _fadeInOutAlphaDelta;
		
		public static LoadingScreenController Instance { get; private set; }

		public async Task FadeIn(bool showLogo = true)
		{
			GameStateMachine.CurrentGameState = GameStateMachine.GameState.Loading;
			LoadingScreen.blocksRaycasts = true;
			LoadingScreen.interactable = true;
			while (LoadingScreen.alpha < 1)
			{
				LoadingScreen.alpha += _fadeInOutAlphaDelta;
				await new WaitForSeconds(_fadeInOutTimeDelta);
			}

			LoadingScreen.alpha = 1;
		}

		public async Task FadeOut(bool showLogo = true)
		{
			while (LoadingScreen.alpha > 0)
			{
				LoadingScreen.alpha -= _fadeInOutAlphaDelta;
				await new WaitForSeconds(_fadeInOutTimeDelta);
			}
			
			LoadingScreen.alpha = 0;
			LoadingScreen.blocksRaycasts = false;
			LoadingScreen.interactable = false;
			GameStateMachine.CurrentGameState = GameStateMachine.GameState.Running;
		}
		
		private void Awake()
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}

		private void Start()
		{
			_fadeInOutTimeDelta = FadeInOutTime / FadeInOutFrames;
			_fadeInOutAlphaDelta = 1 / FadeInOutFrames;
		}
	}
}
