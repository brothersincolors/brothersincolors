﻿using Controllers.GameControllers;
using Events;
using UnityEngine;

namespace Controllers.Cameras
{
    public class GameplayCameraController : MonoBehaviour {
    
        public float YOffset;

        public static GameplayCameraController Instance { get; private set; }

        private bool _changingPosition;
        private Vector3 _lastCheckpointPosition;

        public Camera GetCamera()
        {
            return GetComponent<Camera>();
        }

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            EventAggregator.SubscribeToPlayerDeath(OnPlayerDeath);
        }

        private void FixedUpdate()
        {
            if (_changingPosition)
            {
                transform.position = Vector3.Lerp(transform.position, _lastCheckpointPosition, 0.5f);
                if (transform.position == _lastCheckpointPosition)
                {
                    _changingPosition = false;
                }
                return;
            }
            var position = transform.position;
            if (MainGameplayController.Instance.Players == null) return;

            var minYVal = Mathf.Min(MainGameplayController.Instance.Players[0].transform.position.y, 
                MainGameplayController.Instance.Players[1].transform.position.y, 
                MainGameplayController.Instance.Players[2].transform.position.y);

            var maxYVal = Mathf.Max(MainGameplayController.Instance.Players[0].transform.position.y, 
                MainGameplayController.Instance.Players[1].transform.position.y, 
                MainGameplayController.Instance.Players[2].transform.position.y);

            position.y = (minYVal + maxYVal) / 2 + YOffset;

            var minXVal = Mathf.Min(MainGameplayController.Instance.Players[0].transform.position.x,
                MainGameplayController.Instance.Players[1].transform.position.x, 
                MainGameplayController.Instance.Players[2].transform.position.x);

            var maxXVal = Mathf.Max(MainGameplayController.Instance.Players[0].transform.position.x, 
                MainGameplayController.Instance.Players[1].transform.position.x, 
                MainGameplayController.Instance.Players[2].transform.position.x);

            position.x = (minXVal + maxXVal) / 2;

            var leftCollisoin = false;
            var rightCollision = false;

            var attCamera = GetComponent<Camera>();
            var rightRay = new Ray2D(attCamera.ScreenToWorldPoint(new Vector3(attCamera.pixelWidth, attCamera.pixelHeight, 0)), Vector2.down);
            var leftRay = new Ray2D(attCamera.ScreenToWorldPoint(new Vector3(0, attCamera.pixelHeight, 0)), Vector2.down);

            var hit = Physics2D.Raycast(leftRay.origin, leftRay.direction, Mathf.Infinity, LayerMask.NameToLayer("Default"));
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    leftCollisoin = true;
                }
            }

            hit = Physics2D.Raycast(rightRay.origin, rightRay.direction, Mathf.Infinity, LayerMask.NameToLayer("Default"));
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    rightCollision = true;
                }
            }

            if (leftCollisoin && rightCollision) return;

            transform.position = Vector3.Lerp(transform.position, position, 0.3f);
        }

        private void OnDestroy()
        {
            EventAggregator.UnsubscribeFromPlayerDeath(OnPlayerDeath);
        }

        private void OnPlayerDeath()
        {
            _changingPosition = true;
            _lastCheckpointPosition = MainGameplayController.LastCheckpointPosition;
            _lastCheckpointPosition.z = transform.position.z;
            _lastCheckpointPosition.y += YOffset;
        }
    }
}