﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controllers.Cameras
{
    public class ScreenOrigin : MonoBehaviour
    {

        public Vector3 BottomLeft;
        public Vector3 TopRight;

        public readonly Transform Target;
        public Vector3 ScreenSize;

        public ScreenOrigin(Transform target, Vector3 screenSize)
        {
            this.Target = target;
            ScreenSize = screenSize;
            RecalculateOrigins();
        }

        public void RecalculateOrigins()
        {
            BottomLeft = Target.transform.position - ScreenSize / 2f;
            TopRight = Target.transform.position + ScreenSize / 2f;
        }

        public bool Contains(Vector3 point)
        {
            return point.x >= BottomLeft.x && point.x <= TopRight.x && point.y >= BottomLeft.y && point.y <= TopRight.y;
        }

        public void DrawOrigins()
        {
            Debug.DrawLine(BottomLeft, TopRight, new Color(1f, 0.21f, 0.3f));
        }
    }
}