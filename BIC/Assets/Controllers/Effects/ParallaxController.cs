﻿using UnityEngine;

namespace Controllers.Effects
{
	public class ParallaxController : MonoBehaviour
	{

		public Transform TargetTransform;
		public Vector2 Offset;
		[Range(50, 1000)]
		public float SpeedDivider = 100;
		public float Speed = 0.1f;
		
		private Material _material;
		private Vector3 _worldOffset;
		
		private void Start ()
		{
			_material = GetComponent<Renderer>().material;
		}
	
		private void Update () 
		{
//			_material.mainTextureOffset += new Vector2(Speed, 0) * Time.deltaTime;
			_material.mainTextureOffset = (_worldOffset - TargetTransform.position) / SpeedDivider + (Vector3) Offset;
		}
	}
}
