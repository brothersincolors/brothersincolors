﻿using Controllers.GameControllers;
using Events;
using RaycastPhysics;
using UnityEngine;

namespace Controllers.Effects
{

    public class BloodCompatible : MonoBehaviour
    {
        public float MaxLifeLoss = 3;
        public float MinLifeLoss = 0.6f;

        [Header("Debug")]
        public bool DrawBoundingBox;

        private SpriteRenderer _attRenderer;
        private BoundingBox2D _bounds;

        private bool _visibleByCamera;

        private void OnBecameVisible()
        {
            _visibleByCamera = true;
        }

        private void OnBecameInvisible()
        {
            _visibleByCamera = false;
        }

        private void Start()
        {
            _attRenderer = GetComponent<SpriteRenderer>();
            _bounds = new BoundingBox2D(transform, _attRenderer.sprite);
            SetOwnSprite();
            EventAggregator.SubscribeToPlayerDeath(OnPlayerDeath);
        }

        private void SetOwnSprite()
        {
            // Creating texture
            var tex = new Texture2D((int) _attRenderer.sprite.rect.width, (int) _attRenderer.sprite.rect.height,
                                          _attRenderer.sprite.texture.format, false);
            // Variables for tiling texture
            var border = _attRenderer.sprite.border;
            var rect = _attRenderer.sprite.rect;
            var startTexture = _attRenderer.sprite.texture;
            var tiledTexturePixels = new Color[tex.width * tex.height];
            var startPixels = startTexture.GetPixels();

            for (int i = 0; i < (tex.width * tex.height); i++)
                tiledTexturePixels[i] = Color.white;

            // Filling texture with tiling
            int column, row;
            int localY = (int)_attRenderer.sprite.rect.yMin;
            int localX = (int)_attRenderer.sprite.rect.xMin;

            row = localY;
            column = localX;
            Color color = _attRenderer.color;
            for (int i = 0; i < tex.width * tex.height; i++)
            {
                tiledTexturePixels[i] = startPixels[row * startTexture.width + column];
                column++;
                if (column >= localX + rect.width)
                {
                    row++;
                    column = localX;
                }

                tiledTexturePixels[i].r = color.r * tiledTexturePixels[i].r;
				tiledTexturePixels[i].g = color.g * tiledTexturePixels[i].g;
				tiledTexturePixels[i].b = color.b * tiledTexturePixels[i].b;
			}
            _attRenderer.color = Color.white;
            tex.SetPixels(tiledTexturePixels);
            tex.filterMode = FilterMode.Point;
            tex.Apply();

            var sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f), _attRenderer.sprite.pixelsPerUnit,
                                       1, SpriteMeshType.FullRect, _attRenderer.sprite.border);
            sprite.name = gameObject.name;
            _attRenderer.sprite = sprite;
            _bounds.ChangeSprite(sprite);
        }

        private bool _shouldUpdate;

        private void Update()
        {
            _bounds.UpdateOrigins();
            _attRenderer.color = Color.white;
            if (_visibleByCamera)
            {
                if (_shouldUpdate)
                {
                    var list = new ParticleSystem.Particle[PlayerDeathEffectController.Instance.ParticlesAmount];
                    int n = PlayerDeathEffectController.Instance.PlayerDeathPs.GetParticles(list);
                    _shouldUpdate = false;
                    for (int i = 0; i < n; i++)
                    {
                        DrawBloodTrack(ref list[i]);
//                        _shouldUpdate |= list[i].remainingLifetime > 0;
                            
                    }
                    PlayerDeathEffectController.Instance.PlayerDeathPs.SetParticles(list, n);
                    
                    var _list = new ParticleSystem.Particle[PlayerDeathEffectController.Instance.ParticlesAmount];
                    var _n = PlayerDeathEffectController.Instance.PlayerDeathPsSubemitter.GetParticles(_list);
                    for (int i = 0; i < _n; i++)
                    {
                        DrawBloodTrack(ref _list[i]);
                        _shouldUpdate |= _list[i].remainingLifetime > 0;    
                    }
                    PlayerDeathEffectController.Instance.PlayerDeathPsSubemitter.SetParticles(_list, _n);
                }
            }

            if (DrawBoundingBox)
                _bounds.DrawSelf();
		}

        private void DrawBloodTrack(ref ParticleSystem.Particle particle)
        {
            var point = particle.position;
            var bloodColor = particle.startColor;
            if (_bounds.Contains(point))
			{
                particle.remainingLifetime /= Random.Range(MinLifeLoss, MaxLifeLoss);
                var textureHeightWorldSpace = _attRenderer.sprite.texture.height / _attRenderer.sprite.pixelsPerUnit;
				var up = Vector3.ClampMagnitude(transform.up.normalized, textureHeightWorldSpace / 2);
                var centerOffset = new Vector2(point.x - (transform.position.x + up.x), point.y - (transform.position.y + up.y))
                    * _attRenderer.sprite.pixelsPerUnit;
                if (Mathf.Abs(up.y) < 1 / _attRenderer.sprite.pixelsPerUnit)
                {
                    if (up.x < 0)
                    {
                        var copy = centerOffset;
						centerOffset.x = copy.y;
						centerOffset.y = -copy.x;
                    }
                    if (up.x > 0)
                    {
                        var copy = centerOffset;
                        centerOffset.x = -copy.y;
                        centerOffset.y = copy.x;
                    }
                } else
                    if (up.y < 0) centerOffset *= -1;
                centerOffset += new Vector2(_attRenderer.sprite.texture.width / 2, _attRenderer.sprite.texture.height);
                var col = _attRenderer.sprite.texture.GetPixel((int)centerOffset.x, (int)centerOffset.y);
                
			    if (col != MainGameplayController.Instance.Colors["Red"] 
			        && col != MainGameplayController.Instance.Colors["Green"] 
                    && col != MainGameplayController.Instance.Colors["Blue"]
                    && col != MainGameplayController.Instance.Colors["Magenta"] 
			        && col != MainGameplayController.Instance.Colors["Yellow"] 
			        && col != MainGameplayController.Instance.Colors["Cyan"]) col = Color.clear;
				
			    _attRenderer.sprite.texture.SetPixel((int)centerOffset.x, (int)centerOffset.y, col + bloodColor);
				_attRenderer.sprite.texture.Apply();
			}
        }


        private void OnPlayerDeath()
        {
            _shouldUpdate = true;
        }
    }
}