﻿using UnityEngine;

namespace Controllers.Effects
{
	[RequireComponent(typeof(ParticleSystem))]
	public class PlayerDeathEffectController : MonoBehaviour
	{

		public int ParticlesAmount = 30;
		
		private ParticleSystem _playerDeathPS;
		private ParticleSystem _playerDeathPSSubemitter;

		public ParticleSystem PlayerDeathPs
		{
			get { return _playerDeathPS; }
		}

		public ParticleSystem PlayerDeathPsSubemitter
		{
			get { return _playerDeathPSSubemitter; }
		}

		public static PlayerDeathEffectController Instance { get; private set; }
		
		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			_playerDeathPS = GetComponent<ParticleSystem>();
			_playerDeathPSSubemitter = transform.GetChild(0).GetComponent<ParticleSystem>();
		}

		public void EmitBlood(Vector3 position, Color color)
		{
			transform.position = position;
			var psMainModule = _playerDeathPS.main;
			psMainModule.startColor = color;
			var subPSMainModule = _playerDeathPSSubemitter.main;
			subPSMainModule.startColor = color;
			_playerDeathPS.Emit(ParticlesAmount);
			_playerDeathPSSubemitter.Emit(ParticlesAmount);
		}
	}
}
