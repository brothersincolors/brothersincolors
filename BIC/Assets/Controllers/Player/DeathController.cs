﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Events;



namespace Controllers.Player
{

    public class DeathController : MonoBehaviour
    {
        private int _сounterDeath = 0;
        public int CounterDeath
        {
            get { return _сounterDeath; }
            private set { _сounterDeath++; }
        }

        private void Start()
        {
            EventAggregator.SubscribeToPlayerDeath(() => CounterDeath++);
        }
    }

}