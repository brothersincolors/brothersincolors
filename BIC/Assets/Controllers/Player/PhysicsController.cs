﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace RaycastPhysics
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class PhysicsController : MonoBehaviour
    {
        [Header("Physics Settings")]
        public float skinWidth = 0.015f;        // Лучи кастуются с отступом
        public int horizontalRaysCount = 3;     // Кол-во лучей в горизонте
        public int verticalRaysCount = 3;       // Кол-во лучей в вертикали
        public float gravity = -9.8f;           // Гравитация
        public float friction = 0.01f;          // Трение
        public LayerMask collisionMask;         // То с чем сталкиваемся
        public String[] ignoreTags;             // Игнорим некоторые тэги
        public CollisionsInfo collisions;       // Инфа о столкновениях

        [Header("Debug")]
        public bool showVerticalRays;
        public bool showHorizontalRays;

        private RaycastOrigins raycastOrigins;      // То откуда кастуем лучи
        private BoxCollider2D attCollider;
        private Vector3 velocity;
        private Vector3[] offsets = new Vector3[4];     // Сдвиги от центра до точки каста лучей)

        private float horizontalRaySpacing;         // Пропуск места от луча до луча
        private float verticalRaySpacing;           // Аналог.

        private bool _boxColliderChanged = false;                      
        private Vector2 _preRollSizeCollider;
        private Vector2 _preRollOffsetCollider;                  

        public void Move(Vector3 moveInput)
        {
            if (moveInput.x != 0)
                collisions.Reset();
            velocity.x += moveInput.x;
        }

        public void Jump(float jumpPower)
        {
            if (Below(new Vector3(0, skinWidth * 2 * Math.Sign(gravity))))
            {
                velocity.y = jumpPower;
            }
            else
            {
                SideJump(jumpPower);
            }
        }

        public void SideJump(float jumpPower)
        {
            if (SideCollision(Vector3.right * skinWidth))
            {
                AddForce(new Vector3(jumpPower * Mathf.Sign(gravity) / 1.7f, 0), ForceMode2D.Impulse);
                velocity.y = jumpPower;
            } else if (SideCollision(Vector3.left * skinWidth))
            {
                AddForce(new Vector3(-jumpPower * Mathf.Sign(gravity) / 1.7f, 0), ForceMode2D.Impulse);
                velocity.y = jumpPower;
            }
        }

        public void ChangeBoxCollider(Vector2 size, Vector2 offset, float time)
        {
            if (_boxColliderChanged) return;
            _preRollSizeCollider = attCollider.size;
            _preRollOffsetCollider = attCollider.offset;
            ChangeBoxColliderWrapper(size, offset);
            GetBounds();
            CalculateRaySpacing();
            _boxColliderChanged = true;
            Invoke("CancelChangeBoxCollider", time);
            //print("postinvoke");
        }

        private void CancelChangeBoxCollider()
        {
            ChangeBoxColliderWrapper(_preRollSizeCollider, _preRollOffsetCollider);
            GetBounds();
            CalculateRaySpacing();
            _boxColliderChanged = false;
        }

        private void ChangeBoxColliderWrapper(Vector2 size, Vector2 offset)
        {
            attCollider.offset = offset;
            attCollider.size = size;
        }

        // Пинки работают при помощи корутин
        private Coroutine forceCoroutine = null;

        public void AddForce(Vector3 force, ForceMode2D forceMode)
        {
            if (forceCoroutine != null)
            {
                StopCoroutine(forceCoroutine);
                forceCoroutine = StartCoroutine(ForceAdder(force, forceMode));
            }
            else
                forceCoroutine = StartCoroutine(ForceAdder(force, forceMode));
        }

        IEnumerator ForceAdder(Vector3 force, ForceMode2D forceMode)
        {
            var tempForce = force;
            while (Mathf.Abs(tempForce.x) > friction)
            {
                tempForce.x -= friction * Mathf.Sign(force.x);
                velocity += tempForce;
                yield return new WaitForFixedUpdate();
            }
            forceCoroutine = null;
        }

        void Start()
        {
            attCollider = GetComponent<BoxCollider2D>();
            GetBounds();
            CalculateRaySpacing();
            collisions.Reset();
            Physics2D.gravity *= -1;
        }

        private void FixedUpdate()
        {
            CalculateGravity();
        }

        public void CalculateGravity()
        {
            UpdateRaycstOrigins();      // Обновляем точки каста лучей
            collisions.Reset();         // СБрасываем инфу о лучах
            velocity.y += gravity * Time.fixedDeltaTime;        // Применяем гравитацию
            if (Math.Abs(velocity.y) > Mathf.Epsilon)       // Производим проверку столкновений
                VerticalCollision(ref velocity);
            if (Math.Abs(velocity.x) > Mathf.Epsilon)       // Производим проверку столкновений
                HorizontalCollision(ref velocity);
            transform.Translate(velocity);      // Движемся в зависимости от нашего направления
            velocity.x = 0;
            if (collisions.below || collisions.above)       // Если под нами что-то есть то не изменяем наше положение по у
                velocity.y = 0f;
        }

        void HorizontalCollision(ref Vector3 _velocity)
        {
            float xDirection = Mathf.Sign(_velocity.x);
            float rayLength = Mathf.Abs(_velocity.x) + skinWidth;
            // Кастуем лучи для проверки столкновений 
            for (int i = 0; i < horizontalRaysCount; i++)
            {
                Vector2 rayOrigin = (xDirection == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
                rayOrigin += Vector2.up * horizontalRaySpacing * i;
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * xDirection, rayLength, collisionMask);
                if (hit && ValidCollider(hit.collider))
                {
                    _velocity.x = (hit.distance - skinWidth) * xDirection;      // Если есть столкновение и то сдвигаемся до этого объекта
                    rayLength = hit.distance;
                    collisions.left = xDirection == -1;
                    collisions.right = xDirection == 1;
                }
                if (showHorizontalRays)
                    Debug.DrawRay(rayOrigin, Vector2.right * xDirection, Color.green);

            }
        }

        void VerticalCollision(ref Vector3 _velocity)
        {
            // Точно так же как и горизональные столкновения
            float yDirection = Mathf.Sign(_velocity.y);
            float rayLength = Mathf.Abs(_velocity.y) + skinWidth;
            for (int i = 0; i < verticalRaysCount; i++)
            {
                Vector2 rayOrigin = (yDirection == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                rayOrigin += Vector2.right * verticalRaySpacing * i;
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * yDirection, rayLength, collisionMask);
                if (hit && ValidCollider(hit.collider))
                {
                    _velocity.y = (hit.distance - skinWidth) * yDirection;
                    rayLength = hit.distance;
                    collisions.below = yDirection == -1;
                    collisions.above = yDirection == 1;
                }
                if (showVerticalRays)
                    Debug.DrawRay(rayOrigin, Vector2.up * yDirection, Color.green);
            }
        }

        public bool Below(Vector3 _velocity)
        {
            // Проверка столкновий с низу 
            float yDirection = Mathf.Sign(_velocity.y);
            float rayLength = Mathf.Abs(_velocity.y) + skinWidth;
            for (int i = 0; i < verticalRaysCount; i++)
            {
                Vector2 rayOrigin = (yDirection == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                rayOrigin += Vector2.right * verticalRaySpacing * i;
                RaycastHit2D[] hit = Physics2D.RaycastAll(rayOrigin, Vector2.up * yDirection, rayLength, collisionMask);
                for (int j = 0; j < hit.Length; j++)
                {
                    if (hit[j] && ValidCollider(hit[j].collider))
                    {
                        _velocity.y = (hit[j].distance - skinWidth) * yDirection;
                        collisions.below = yDirection == -1;
                        collisions.above = yDirection == 1;
                        return true;
                    }
                }
            }
            return false;
        }
        
        public bool SideCollision(Vector3 _velocity)
        {
            float xDirection = Mathf.Sign(_velocity.x);
            float rayLength = Mathf.Abs(_velocity.x) + skinWidth;
            // Кастуем лучи для проверки столкновений 
            for (int i = 0; i < horizontalRaysCount; i++)
            {
                Vector2 rayOrigin = (xDirection == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
                rayOrigin += Vector2.up * horizontalRaySpacing * i;
                RaycastHit2D[] hit = Physics2D.RaycastAll(rayOrigin, Vector2.right * xDirection, rayLength, collisionMask);
                for (int j = 0; j < hit.Length; j++)
                {
                    if (hit[j] && ValidCollider(hit[j].collider))
                    {
                        _velocity.x = (hit[j].distance - skinWidth) * xDirection;
                        collisions.below = xDirection == -1;
                        collisions.above = xDirection == 1;
                        return true;
                    }
                }
            }

            return false;
        }

        public bool OnGround()
        {
            return velocity.y == 0;
        }

        void UpdateRaycstOrigins()      // Обновление вершин для каста лучей
        {
            raycastOrigins.topLeft = transform.position - offsets[0];
            raycastOrigins.bottomLeft = transform.position - offsets[1];
            raycastOrigins.bottomRight = transform.position - offsets[2];
            raycastOrigins.topRight = transform.position - offsets[3];
        }

        void CalculateRaySpacing()      // Высчитываем пробелы между лучами
        {
            Bounds bounds = attCollider.bounds;
            bounds.Expand(skinWidth * -2);

            horizontalRaysCount = Mathf.Clamp(horizontalRaysCount, 2, int.MaxValue);
            verticalRaysCount = Mathf.Clamp(verticalRaysCount, 2, int.MaxValue);

            horizontalRaySpacing = bounds.size.y / (horizontalRaysCount - 1);
            verticalRaySpacing = bounds.size.x / (verticalRaysCount - 1);
        }

        bool ValidCollider(Collider2D coll)     // Можно ли сталкиваться с таким коллайдером
        {
            if (coll.isTrigger) return false;
            for (int i = 0; i < ignoreTags.Length; i++)
                if (ignoreTags[i] == coll.tag) return false;
            return true;
        }

        private void GetBounds()        // Вытаскиваем значения сдвигов для нашего Bounds
        {
            Bounds bounds = attCollider.bounds;
            bounds.Expand(skinWidth * -2);
            raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
            raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
            raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
            raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
            offsets[0] = new Vector3(transform.position.x - raycastOrigins.topLeft.x, transform.position.y - raycastOrigins.topLeft.y);
            offsets[1] = new Vector3(transform.position.x - raycastOrigins.bottomLeft.x, transform.position.y - raycastOrigins.bottomLeft.y);
            offsets[2] = new Vector3(transform.position.x - raycastOrigins.bottomRight.x, transform.position.y - raycastOrigins.bottomRight.y);
            offsets[3] = new Vector3(transform.position.x - raycastOrigins.topRight.x, transform.position.y - raycastOrigins.topRight.y);
        }

        private void OnDisable()
        {
            velocity = Vector3.zero;
        }

    }

    struct RaycastOrigins       // Хранит данные о точках
    {
        public Vector2 topLeft;
        public Vector2 bottomLeft;
        public Vector2 bottomRight;
        public Vector2 topRight;
    }
    [Serializable]
    public struct CollisionsInfo        // Информация от столкновениях
    {
        public bool left, right;
        public bool above, below;

        public void Reset()
        {
            left = right = above = below = false;
        }
    }

    struct BoundingBox2D        // Необходима для крови 
    {
        public Transform transform;

        Sprite sprite;
        BoundingBoxOrigins2D origins;

        public float height;
        public float width;

        public Sprite Sprite
        {
            get
            {
                return sprite;
            }
        }

        public BoundingBox2D(Transform _transform, Sprite _sprite)
        {
            transform = _transform;
            sprite = _sprite;
            var texture = sprite.texture;
            height = texture.height / sprite.pixelsPerUnit;
            width = texture.width / sprite.pixelsPerUnit;
            origins = new BoundingBoxOrigins2D();
            UpdateOrigins();
        }

        public void ChangeSprite(Sprite _sprite)
        {
            sprite = _sprite;
            var texture = _sprite.texture;
            height = texture.height / _sprite.pixelsPerUnit;
            width = texture.width / _sprite.pixelsPerUnit;
            UpdateOrigins();
        }

        public void UpdateOrigins()
        {
            Vector3 up = transform.up.normalized * (height * transform.lossyScale.y / 2);
            Vector3 right = transform.right.normalized * (width * transform.lossyScale.x / 2);
            origins.vertices[0] = transform.position + up + right;
            origins.vertices[1] = transform.position + right - up;
            origins.vertices[2] = transform.position - up - right;
            origins.vertices[3] = transform.position + up - right;
        }

        public void DrawSelf() { origins.DrawOrigins(); }

        public bool Contains(Vector3 point) { return origins.Contains(point, Mathf.Epsilon); }

        class BoundingBoxOrigins2D
        {
            public Vector3[] vertices;

            public BoundingBoxOrigins2D() { vertices = new Vector3[4]; }

            public bool Contains(Vector3 point, float epsilon)
            {
                float minX = Mathf.Min(new float[] { vertices[0].x, vertices[1].x, vertices[2].x, vertices[3].x });
                float minY = Mathf.Min(new float[] { vertices[0].y, vertices[1].y, vertices[2].y, vertices[3].y });
                float maxX = Mathf.Max(new float[] { vertices[0].x, vertices[1].x, vertices[2].x, vertices[3].x });
                float maxY = Mathf.Max(new float[] { vertices[0].y, vertices[1].y, vertices[2].y, vertices[3].y });
                if ((point.x > minX && point.x < maxX && point.y > minY && point.y < maxY)
                    || Math.Abs(point.x - minX) < epsilon || Math.Abs(point.x - maxX) < epsilon
                    || Math.Abs(point.y - minY) < epsilon || Math.Abs(point.y - maxY) < epsilon) return true;
                return false;
            }

            public void DrawOrigins()
            {
                Debug.DrawLine(vertices[0], vertices[1], Color.red);
                Debug.DrawLine(vertices[1], vertices[2], Color.red);
                Debug.DrawLine(vertices[2], vertices[3], Color.red);
                Debug.DrawLine(vertices[3], vertices[0], Color.red);
            }
        }
    }

}