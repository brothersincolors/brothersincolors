﻿using System.Collections;
using System.Collections.Generic;
using Controllers.GameControllers;
using UnityEngine;


namespace Controllers.Player
{

    public class GlowController : MonoBehaviour
    {
        public bool EnableOnStart = false;
        public float Intensity = 10;
        public float Duration = 1;

        private Light _attLight;
        private float _enableDurationTime = 0;
        private float _disableDurationTime = 0;

        private void Start()
        {
            _attLight = gameObject.GetComponent<Light>();
            _attLight.intensity = EnableOnStart ? Intensity : 0;
            _attLight.enabled = false;
        }

        private void FixedUpdate()
        {
            if (MainGameplayController.White)
            {
                if (_enableDurationTime == -1) _enableDurationTime = Time.time;

                _attLight.enabled = true;
                _attLight.intensity = Mathf.Lerp(_attLight.intensity, Intensity, (Time.time - _enableDurationTime) / Duration);

                float positionZ = transform.position.z;
                transform.position = Vector3.zero;
                for (int i = 0; i < 3; i++)
                    transform.position += MainGameplayController.Instance.PlayerControllers[i].transform.position / 3;

                transform.position = new Vector3(transform.position.x, transform.position.y, positionZ);
                _disableDurationTime = -1;
            }
            else
            {

                StartCoroutine(WaitTwoFrames());

            }
        }
        IEnumerator WaitTwoFrames()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();

            if (_disableDurationTime == -1) _disableDurationTime = Time.time;
            _attLight.intensity = Mathf.Lerp(_attLight.intensity, 0, (Time.time - _disableDurationTime) / Duration);
            _enableDurationTime = -1;
        }
    }
}