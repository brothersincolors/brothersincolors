﻿using Configs.Player;
using Controllers.GameControllers;
using Extensions;
using UnityEngine;

namespace Controllers.Player
{
	public class ColorChanger : MonoBehaviour
	{
		public Collider2D MyCollider;
		public Collider2D[] OtherColliders = new Collider2D[2];
		public Color PlayerColor;
		public PlayerColor Col;

		PlayerController _player;

		private int _contact;
		private SpriteRenderer _playerRenderer;

		private void Start()
		{
			_player = transform.parent.GetComponent<PlayerController>();
			PlayerColor = _player.Color;
			MyCollider = GetComponent<Collider2D>();
			_playerRenderer = transform.parent.gameObject.GetComponent<SpriteRenderer>();
		}

		private void Update()
		{
			if (_contact > 0)
			{
				var firstCollision = Physics2D.IsTouching(MyCollider, OtherColliders[0]);
				var secondCollision = Physics2D.IsTouching(MyCollider, OtherColliders[1]);

				if (firstCollision && !secondCollision)
				{
					_playerRenderer.color = ColorSumm(Col.Name, OtherColliders[0].GetComponent<ColorChanger>().Col.Name);
				}

				if (secondCollision && !firstCollision)
				{
					_playerRenderer.color = ColorSumm(Col.Name, OtherColliders[1].GetComponent<ColorChanger>().Col.Name);
				}

				if (secondCollision && firstCollision)
				{
					MainGameplayController.White = true;
				}

				if (MainGameplayController.White)
					_playerRenderer.color = Color.white;
			}
			else
			{
				_playerRenderer.color = PlayerColor;
			}

			CheckLayer();
		}

		private void OnTriggerEnter2D(Collider2D coll)
		{
			if (coll.CompareTag("Player"))
			{
				_contact++;
			}
		}

		private void OnTriggerExit2D(Collider2D coll)
		{
			if (coll.CompareTag("Player"))
			{
				_contact--;
				if (_playerRenderer != null)
					_playerRenderer.color = PlayerColor;
				MainGameplayController.White = false;
			}
		}

		private static Color ColorSumm(string a, string b)
		{
			if (a == "Red" || b == "Red")
			{
				if (b == "Green" || a == "Green")
					return MainGameplayController.Instance.Colors["Yellow"];
				if (b == "Blue" || a == "Blue")
					return MainGameplayController.Instance.Colors["Magenta"];
			}

			if (a == "Blue" || b == "Blue")
			{
				if (a == "Green" || b == "Green")
					return MainGameplayController.Instance.Colors["Cyan"];
			}

			return MainGameplayController.Instance.Colors[a].Summ(MainGameplayController.Instance.Colors[b]);
		}

		private void CheckLayer()
		{
			if (_playerRenderer.color == MainGameplayController.Instance.Colors["Red"])
				_player.ChangeLayer("Red");
			else if (_playerRenderer.color == MainGameplayController.Instance.Colors["Green"])
				_player.ChangeLayer("Green");
			else if (_playerRenderer.color == MainGameplayController.Instance.Colors["Blue"])
				_player.ChangeLayer("Blue");
			else if (_playerRenderer.color == MainGameplayController.Instance.Colors["Yellow"])
				_player.ChangeLayer("Yellow");
			else if (_playerRenderer.color == MainGameplayController.Instance.Colors["Magenta"])
				_player.ChangeLayer("Magenta");
			else if (_playerRenderer.color == MainGameplayController.Instance.Colors["Cyan"])
				_player.ChangeLayer("Cyan");
			else if (_playerRenderer.color == MainGameplayController.Instance.Colors["White"])
				_player.ChangeLayer("White");
		}
	}
}