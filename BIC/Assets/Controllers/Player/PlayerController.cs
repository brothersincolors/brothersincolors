﻿using Configs;
using Configs.Player;
using Controllers.GameControllers;
using Controllers.Traps;
using DataBases.KeycodesDataBase;
using DataBases.SkinDataBase;
using Extensions;
using InputControl;
using RaycastPhysics;
using UnityEngine;

namespace Controllers.Player
{

    public class PlayerController : MonoBehaviour
    {

        [Header("Player Behaviour")] public Color Color;
        public int Index;
        public float PunchForce = 0.2f; // Сила удара

        [Header("Physics")] public float Speed = 1;
        public float JumpPower = 1;
        public float TimeRoll = 2;
        public float RollScale = 0.5f;

        private InputDevice _device;
        private Animator _attAnimator;
        private PhysicsController _attController;
        private BoxCollider2D _attCollider;
        private SpriteRenderer _attRenderer;

        private Vector3 _moveInput;

        private LeverController _contactedLeverController; // Рычаг возле которого мы стоим
        private PlayerController _contactedPlayer; // Игрок возле которого мы стоим

        private PlayerKeyboardControls _playerKeyboardControls;

        public float JumpPowerCopy;

        public Color CurrentColor => _attRenderer.color;

        public void SetInputDevice(InputDevice device, PlayerKeyboardControls playerKeyboardControls)
        {
            _device = device;
            _playerKeyboardControls = playerKeyboardControls;
        }

        public void RemoveInputDevice()
        {
            _device = null;
            _playerKeyboardControls = null;
        }

        public int FlipGravity()
        {
            _attController.gravity = _attController.gravity * -1;
            _attRenderer.flipY = !_attRenderer.flipY;
            return (int)Mathf.Sign(_attController.gravity);
        }


        private void SetSkin(SkinModel skin)
        {
            if (!_attAnimator)
                _attAnimator = GetComponent<Animator>();
            _attAnimator.runtimeAnimatorController = skin.RuntimeAnimatorController;
        }

        private void Punch(Vector2 diretcion, ForceMode2D forceMode) // Пинок другого игрока
        {
            _attController.AddForce(diretcion, forceMode);
        }

        private void Jump(float jumpPower)
        {
             _attController.Jump(jumpPower * Time.fixedDeltaTime * -1 * Mathf.Sign(_attController.gravity));
            _attAnimator.SetBool("Jump", true);
            JumpPower = JumpPowerCopy;
        }

        private void Roll(Vector2 size, Vector2 offset, float time) => _attController.ChangeBoxCollider(size, offset, time);

        private void OnEnable()
        {
            Invoke(nameof(FixJump), 0.1f);
        }

        private void FixJump()
        {
            JumpPower = JumpPowerCopy;
        }

        private void Start()
        {
            _attController = GetComponent<PhysicsController>();
            _attCollider = GetComponent<BoxCollider2D>();
            _attAnimator = GetComponent<Animator>();
            _attRenderer = GetComponent<SpriteRenderer>();
            _attRenderer.color = Color;
            _attRenderer.sortingOrder = Index + 5;
            JumpPowerCopy = JumpPower;
            SetSkin(SkinDataBase.Instance.GetPlayerSkin(Preferences.Prefs.Session.CurrentSkins[Index]));
        }

        private void Update()
        {
            if (_device != null && _device.DeviceIsConnected)
            {
                if (_contactedLeverController)
                {
                    if (_device.GetKeyDown(GamepadKeycode.Action4))
                        _contactedLeverController.ChangeState(this);
                }

                if (!_contactedLeverController && _contactedPlayer && !MainGameplayController.White)
                {
                    if (_device.GetKeyDown(GamepadKeycode.Action2))
                    {
                        var direction = (_contactedPlayer.transform.position - transform.position).normalized *
                                        PunchForce;
                        direction.y = 0;
                        _contactedPlayer.Punch(direction, ForceMode2D.Force);
                    }
                }

                _moveInput = new Vector3(_device.GetAxis(GamepadKeycode.LeftStickX), 0, 0);
                if (_device.GetKeyDown(GamepadKeycode.Action3))
                {
                    Jump(JumpPower);
                }
            }
            else if (_playerKeyboardControls != null)
            {

                if (_contactedLeverController)
                {
                    if (InputManager.Keyboard.GetKeyDown(_playerKeyboardControls.Interact))
                        _contactedLeverController.ChangeState(this);
                }

                if (!_contactedLeverController && _contactedPlayer && !MainGameplayController.White)
                {
                    if (InputManager.Keyboard.GetKey(_playerKeyboardControls.Interact))
                    {
                        var direction = (_contactedPlayer.transform.position - transform.position).normalized *
                                        PunchForce;
                        direction.y = 0;
                        _contactedPlayer.Punch(direction, ForceMode2D.Force);
                    }
                }

                if (InputManager.Keyboard.GetKeyDown(_playerKeyboardControls.Jump))
                {
                    Jump(JumpPower);
                }

                if (InputManager.Keyboard.GetKeyDown(_playerKeyboardControls.Roll))
                {
                    Roll(new Vector2(_attCollider.size.x, _attCollider.size.y * RollScale), new Vector2(_attCollider.offset.x, _attCollider.offset.y - _attCollider.size.y * (1 - RollScale) / 2), TimeRoll);
                }
                _moveInput = new Vector3(
                    InputManager.Keyboard.GetKey(_playerKeyboardControls.Right).ToInt() -
                    InputManager.Keyboard.GetKey(_playerKeyboardControls.Left).ToInt(), 0, 0);
            }
            else
            {
                _moveInput = Vector3.zero;
            }

            if (_moveInput != Vector3.zero)
            {
                _attRenderer.flipX = !(_moveInput.x > 0);
            }

            _attAnimator.SetBool("Run", _moveInput != Vector3.zero);

            _attAnimator.SetBool("Jump", !_attController.OnGround());
        }

        private void OnDestroy()
        {
            _device?.FreeDevice();
        }

        private void FixedUpdate()
        {
            _attController.Move(_moveInput * Speed * Time.fixedDeltaTime);
        }

        // -------------------------- Collisions ---------------------------//

        private void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.gameObject.CompareTag("Player"))
            {
                _contactedPlayer = coll.gameObject.transform.parent.GetComponent<PlayerController>();
            }
        }

        private void OnTriggerStay2D(Collider2D coll)
        {
            if (coll.CompareTag("Lever"))
                _contactedLeverController = coll.GetComponent<LeverController>();
        }

        private void OnTriggerExit2D(Collider2D coll)
        {
            if (coll.CompareTag("Lever"))
                _contactedLeverController = null;
            if (coll.gameObject.CompareTag("Player"))
            {
                _contactedPlayer = null;
            }
        }

        // --------------------- Layers Control ---------------- //

        private string _previousLayerName;

        public void ChangeLayer(string layer) // Вызывается Color Checker для смены слоя
        {
            if (_previousLayerName == layer) return;
            _previousLayerName = layer;
            _attController.collisionMask = LayerMask.GetMask("Obstacles", layer);
        }

        public DefaultPlayerColor GetPlayerColor()
        {
            if (Color == MainGameplayController.Instance.Colors["Red"])
                return DefaultPlayerColor.Red;
            if (Color == MainGameplayController.Instance.Colors["Green"])
                return DefaultPlayerColor.Green;
            return DefaultPlayerColor.Blue;
        }
    }

}

