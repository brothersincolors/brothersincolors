﻿using System.Collections;
using System.Collections.Generic;
using Configs.Player;
using Controllers.GameControllers;
using UnityEngine;

namespace Controllers.Player
{

    public class SwapColorController : MonoBehaviour
    {
        public Vector3 SizeParicleSystem = new Vector3(0.3f, 0.3f, 0.3f);
        public PlayerColor[] PlayerColors = new PlayerColor[7];

        private List<GameObject> _particleSystems = new List<GameObject>();
        private Dictionary<string, Color> _colors;
        private MainGameplayController _attMainGameplay;
        private int _entranceCounter = 0;

        static SwapColorController _instance;

        public static SwapColorController Instance => _instance;

        public void DefaultColors()
        {
            _entranceCounter = 0;
            Swap(_attMainGameplay.PlayerColors);
        }

        public void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.CompareTag("Player"))
            {
                _entranceCounter++;
                Swap(PlayerColors);
            }
        }

        public void OnTriggerExit2D()
        {
            _entranceCounter--;
            if (_entranceCounter > 0) return;

            DefaultColors();
        }

        public void Swap(PlayerColor[] playerColors)
        {
            _colors = new Dictionary<string, Color>();
            foreach (var i in playerColors)
                _colors.Add(i.Name, i.Color);

            _attMainGameplay.Colors = _colors;

            for (var i = 0; i < 3; i++)
            {
                _attMainGameplay.PlayerControllers[i].Color = playerColors[i].Color;
                _attMainGameplay.Players[i].GetComponent<SpriteRenderer>().color = playerColors[i].Color;
                _attMainGameplay.PlayerChangers[i].PlayerColor = playerColors[i].Color;
                _attMainGameplay.PlayerChangers[i] =
                    _attMainGameplay.Players[i].transform.GetChild(0).GetComponent<ColorChanger>();
                _attMainGameplay.PlayerChangers[i].Col = playerColors[i];
            }
        }

        private void Awake()
        {
            _instance = this;

        }

        private void Start()
        {
            _attMainGameplay = MainGameplayController.Instance;
            for (int i = 0; i < 3; i++)
            {
                _particleSystems.Add(transform.GetChild(i).gameObject);
                _particleSystems[i].transform.localScale = SizeParicleSystem;
            }

        }
    }
}