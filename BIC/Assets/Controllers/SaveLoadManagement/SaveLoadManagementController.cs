﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Controllers.SaveLoadManagement
{
    public static class SaveLoadManagementController
    {
        /* Обычная сохранялка файлов. Сэйвит в Документы. Для каждого нового типа данных перегружаю метод */

        static readonly BinaryFormatter BinaryFormatter = new BinaryFormatter();

        // Video data
        public static void Save(VideoSettingsData data)
        {
            FileStream stream = File.Create(Application.persistentDataPath + "/vdset.config");
            BinaryFormatter.Serialize(stream, data);
            stream.Close();
        }

        public static bool Load(ref VideoSettingsData data)
        {
            if (!File.Exists(Application.persistentDataPath + "/vdset.config")) return false;
            FileStream stream = File.Open(Application.persistentDataPath + "/vdset.config", FileMode.Open);
            data = (VideoSettingsData)BinaryFormatter.Deserialize(stream);
            stream.Close();
            return true;
        }

        public static void Save(int levelIndex)
        {
			FileStream stream = File.Create(Application.persistentDataPath + "/lv.config");
            BinaryFormatter.Serialize(stream, levelIndex);
			stream.Close();
        }

		public static bool Load(ref int levelIndex)
		{
			if (!File.Exists(Application.persistentDataPath + "/lv.config")) return false;
			FileStream stream = File.Open(Application.persistentDataPath + "/lv.config", FileMode.Open);
            levelIndex = (int) BinaryFormatter.Deserialize(stream);
			stream.Close();
			return true;
		}

        public static void Save(int[] suitParams)
        {
			FileStream stream = File.Create(Application.persistentDataPath + "/si.config");
            BinaryFormatter.Serialize(stream, suitParams);
			stream.Close();
        }

		public static bool Load(ref int[] suitParams)
		{
			if (!File.Exists(Application.persistentDataPath + "/si.config")) return false;
			FileStream stream = File.Open(Application.persistentDataPath + "/si.config", FileMode.Open);
			suitParams = (int[] )BinaryFormatter.Deserialize(stream);
			stream.Close();
			return true;
		}

        public static void ClearVideoSettings() { File.Delete(Application.persistentDataPath + "/vdset.config");}
    }

	[Serializable]
	public class VideoSettingsData
	{
        public SerializableResolution Resolution;
		public bool FullScreen;
		public int VSync;
	}

    // Разрешение экрана, которое можно сохранять в файл
	[Serializable]
	public class SerializableResolution
	{
		public int Width;
		public int Height;

        public SerializableResolution(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }
        // Перевод разрешения в текст для Dropdown
        public static string ResolutionToString(Resolution resolution)
        {
            string text = resolution.ToString();
            string result = "";
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i + 1] == '@') return result;
                result += text[i];
            }
            return result;
        }

        public override string ToString()
        {
            return Width + " x " + Height;
        }
	}
}
