﻿using Controllers.GameControllers;
using UnityEngine;

namespace Controllers.Parallax
{
    public class ParallaxController : MonoBehaviour {

        [Range (0, 50)]
        public float ParallaxSpeed = 1;
        public Vector3 Offset;

        private Transform _cameraTransform;

        void Start()
        {
            _cameraTransform = MainGameplayController.Instance.transform;
        }

        void Update()
        {
            CalculateOffset();
        }

        void CalculateOffset()      // Двигаем задний фон с учетом сдвига и скорости
        {
            var distance = _cameraTransform.position - Vector3.zero;
            distance /= ParallaxSpeed;
            distance.z = transform.position.z;
            transform.position = distance + Offset;
        }
    }
}
