﻿using Controllers.Traps.BaseClasses;

namespace Controllers.Traps.ActivationSystem
{
    public class ActivationCompatibleObjectController : ColorDepended
    {
        public int ActivatorsCount = 0;
        protected int CurrentActiveActivatorsCount = 0;

        public virtual void Activate()
        {
            CurrentActiveActivatorsCount++;
        }

        public virtual void Deactivate()
        {
            CurrentActiveActivatorsCount--;
        }
    }
}