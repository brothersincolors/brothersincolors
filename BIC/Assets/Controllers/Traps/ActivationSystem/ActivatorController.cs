﻿using Controllers.GameControllers;
using Events;
using UnityEngine;

namespace Controllers.Traps.ActivationSystem
{
    public class ActivatorController : MonoBehaviour
    {
        /* Система активации */

        public ActivationCompatibleObjectController ActivationObjectController;     // Тот кого мы активируем

        protected bool Activated;

        protected virtual void Start()
        {
            EventAggregator.SubscribeToPlayerDeath(RestartSystem);
            ActivationObjectController.ActivatorsCount++;          // Увел кол-во активаторов у активируемого объекта 
        }

        protected virtual void ActivateSystem()     // Вызывается при активации активатора
        {
            if (ActivationObjectController == null) return;

            if (Activated)
            {
                ActivationObjectController.Deactivate();
                Activated = false;
            }
            else
            {
                ActivationObjectController.Activate();
                Activated = true;
            }
        }

        protected void RestartSystem()              // Деактивация системы
        {
            if (Activated)
                ActivateSystem();
        }
    }
}