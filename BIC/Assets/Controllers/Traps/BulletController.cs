﻿using System.Collections;
using Controllers.GameControllers;
using Controllers.Player;
using Controllers.Traps.BaseClasses;
using UnityEngine;

namespace Controllers.Traps
{
    public class BulletController : ColorDepended
    {

        public Sprite Sprite;
        public float Speed;
        public Vector3 Direction;
        public SpriteRenderer RendererToUse;

        private Animator _attAnimator;
        private Collider2D _attCollider;

        protected override void Start()
        {
			AttRenderer = RendererToUse;
			AttRenderer.color = Color;
            _attAnimator = GetComponent<Animator>();
            _attCollider = GetComponent<Collider2D>();
            StartCoroutine(PlayExplosion(10));
        }

        protected virtual void Explode()
        {
            StartCoroutine(PlayExplosion(0f));
        }
        
        protected virtual void DestroySelf()
        {
            _attCollider.enabled = true;
            _attAnimator.enabled = false;
            AttRenderer.sprite = Sprite;
            gameObject.SetActive(false);
        }

        private void FixedUpdate()
        {
            transform.Translate(Direction * Speed * Time.fixedDeltaTime);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer != LayerMask.NameToLayer("Traps"))
            {
                if (collision.CompareTag("Player"))
                {
                    if (AttRenderer.color == new Color32(128, 128, 128, 255) ||
                        AttRenderer.color != collision.gameObject.GetComponent<SpriteRenderer>().color)
                    {
                        MainGameplayController.Instance.RestartFromCheckpoint(collision.GetComponent<PlayerController>());
                    }
                }
                Explode();
            }
        }

        private IEnumerator PlayExplosion(float delay)
        {
            yield return new WaitForSeconds(delay);
            Direction = Vector3.zero;
            _attAnimator.enabled = true;
            _attCollider.enabled = false;
            _attAnimator.SetTrigger("Death");
        }
    }
}