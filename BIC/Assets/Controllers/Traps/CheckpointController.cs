﻿using Controllers.GameControllers;
using Controllers.Player;
using Events;
using UnityEngine;

namespace Controllers.Traps
{
    public class CheckpointController : MonoBehaviour {

        private GameObject[] _players = new GameObject[3];
        private int _playersCount;
        private bool _checkpointIsSet;
        private Animator _attAnimator;
        private SpriteRenderer _chRenderer;
        private string _checkpointColor = "";

        private void Start () 
        {
            _attAnimator = GetComponent<Animator>();
            _chRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
            EventAggregator.SubscribeToPlayerDeath(Reset);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (_checkpointIsSet) return;    

            if (collision.CompareTag("Player"))
            {
                var tempFlag = false;
                for (var i = 0; i < 3; i++)
                {
                    if (_players[i] == collision.gameObject)
                    {
                        tempFlag = true;
                        break;
                    }
                }

                if (!tempFlag)
                {
                    var localIndex = 0;
                    for (var i = 0; i < 3; i++)
                    {
                        if (_players[i] == null)
                        {
                            _players[i] = collision.gameObject;
                            localIndex = i;
                            _playersCount++;
                            break;
                        }
                    }
                    if (_players[localIndex] != null)
                    {
                        if (_checkpointColor == "")
                        {
                            _checkpointColor = _players[localIndex].GetComponent<PlayerController>().GetPlayerColor().ToString();
                        }
                        else
                        {
                            _checkpointColor = ColorSumm(_players[localIndex].GetComponent<PlayerController>().GetPlayerColor().ToString(),
                                _checkpointColor);
                        }
                    }

                }

                if (_playersCount >= 3)
                {
                    MainGameplayController.LastCheckpointPosition = transform.position;
                    _checkpointIsSet = true;
                    _attAnimator.SetTrigger("IsSet");
                }
            }
        }

        private void Update()
        {
            if (_checkpointColor == "") return;
            if (_chRenderer.color != MainGameplayController.Instance.Colors[_checkpointColor])
                _chRenderer.color = Color.Lerp(_chRenderer.color, MainGameplayController.Instance.Colors[_checkpointColor], 0.1f);
        
        }



        public void Reset()
        {
            if (_checkpointIsSet) return;
            _players = new GameObject[3];
            _playersCount = 0;
            _checkpointIsSet = false;
            _checkpointColor = "";
            if (_chRenderer)
                _chRenderer.color = Color.grey;
        }

        public void DebugReset()
        {
            _players = new GameObject[3];
            _playersCount = 0;
            _checkpointIsSet = false;
        }

        private static string ColorSumm(string a, string b)
        {
            if (a == "Red" || b == "Red")
            {
                if (b == "Green" || a == "Green")
                    return "Yellow";
                if (b == "Blue" || a == "Blue")
                    return "Magenta";
            }

            if (a != "Blue" && b != "Blue") return "White";
            if (a == "Green" || b == "Green")
                return "Cyan";
            return "White";
        }
    }
}
