﻿using System.Collections;
using System.Collections.Generic;
using Controllers.GameControllers;
using Controllers.Player;
using UnityEngine;
using Controllers.Traps.BaseClasses;

namespace Controllers.Traps.Beams.BeamColorDepended
{
    public class BeamColorDepended : ColorDepended
    {
        protected ParticleSystem AttParticleSystem;
        private ParticleSystem.MainModule main;

        private Gradient GradientMin;
        private Gradient GradientMax;

       
        protected override void Start()
        {
            AttParticleSystem = GetComponent<ParticleSystem>();
            main = AttParticleSystem.main;

            UpdateGradient();  
            main.startColor = new ParticleSystem.MinMaxGradient(GradientMin, GradientMax);

        }


        protected override void OnValidate()
        {
            if (FindComponentInChildren)
            {
                var tempParticleSystem = transform.GetChild(0).GetComponent<ParticleSystem>();
                if (tempParticleSystem)
                {
                    UpdateGradient();
                    var main = tempParticleSystem.main;
                    main.startColor = new ParticleSystem.MinMaxGradient(GradientMin, GradientMax); 
                }
            }
            else
            {
                var tempParticleSystem = GetComponent<ParticleSystem>();
                if (tempParticleSystem)
                {
                    UpdateGradient();
                    var main = tempParticleSystem.main;              
                    main.startColor = new ParticleSystem.MinMaxGradient(GradientMin, GradientMax);
                }
            }
        }


        private void UpdateGradient()
        {
            GradientMin = new Gradient();
            GradientMin.SetKeys(
                new GradientColorKey[] { new GradientColorKey(Color, 0f), new GradientColorKey(Color, 1f) },
                new GradientAlphaKey[] { new GradientAlphaKey(1, 1), new GradientAlphaKey(1, 1) }
            );
            GradientMax = new Gradient();
            GradientMax.SetKeys(
                new GradientColorKey[] { new GradientColorKey(Color, 0f), new GradientColorKey(Color.white, 1f) },
                new GradientAlphaKey[] { new GradientAlphaKey(1, 1), new GradientAlphaKey(1, 1) }
            );
        }
    }
}