﻿using Controllers.Traps.BaseClasses;
using UnityEngine;

namespace Controllers.Traps.Extensions
{
	public class ForestGunController : GunController {
		protected override void Start()
		{
			base.Start();
			transform.GetChild(1).GetComponent<ColorDepended>().Color = BulletColor;
			transform.GetChild(1).GetComponent<SpriteRenderer>().color = BulletColor;
		}
	}
}
