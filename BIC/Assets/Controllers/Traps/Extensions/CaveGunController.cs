﻿using UnityEngine;

namespace Controllers.Traps.Extensions
{
	public class CaveGunController : GunController
	{

		public Light AttLight;

		protected override void Start()
		{
			base.Start();
			AttLight.color = BulletColor;
		}
	}
}
