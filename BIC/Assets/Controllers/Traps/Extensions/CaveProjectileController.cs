﻿using UnityEngine;

namespace Controllers.Traps.Extensions
{
    public class CaveProjectileController : BulletController {

        public ParticleSystem AttPs;
        public Light AttLight;

        protected override void Start()
        {
            base.Start();
            var main = AttPs.main;
            main.startColor = AttRenderer.color;
            AttLight.color = AttRenderer.color;
        }

        protected override void Explode()
        {
            base.Explode();
            AttPs.gameObject.SetActive(false);
        }

        protected override void DestroySelf()
        {
            base.DestroySelf();
            AttPs.gameObject.SetActive(true);
        }
	
    }
}
