﻿using UnityEngine;

namespace Controllers.Traps
{
    public class PressController : MonoBehaviour
    {

        public float Timing = 1;
        public Collider2D HitCollider;

        Animator _attAnimator;
        Collider2D _attCollider;

        void Start()
        {
            _attAnimator = GetComponent<Animator>();
            _attCollider = GetComponent<Collider2D>();
        }

        public void OnPressHit()
        {
            _attCollider.enabled = true;
            HitCollider.enabled = true;
        }

        public void OnPressClose()
        {
            HitCollider.enabled = false;
            _attCollider.enabled = false;
        }

        private void OnValidate()
        {
            if (Timing <= 0.1)
                Timing = 1;
            if (_attAnimator)
                _attAnimator.SetFloat("Time", Timing);
        }
    }
}
