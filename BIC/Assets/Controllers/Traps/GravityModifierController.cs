﻿using Controllers.Player;
using Controllers.Traps.BaseClasses;
using UnityEngine;

namespace Controllers.Traps
{
	public class GravityModifierController : MonoBehaviour
	{

		public Color Color;
		public SpriteRenderer AttRenderer;
		
		private void Start()
		{
			AttRenderer = GetComponent<SpriteRenderer>();
			AttRenderer.color = Color;
		}


		// Убивает если цвета отличаются
		private void OnTriggerEnter2D(Collider2D collision)
		{
			if (collision.CompareTag("Player"))
			{
				print("SHIT");
				if (Color != collision.gameObject.GetComponent<SpriteRenderer>().color)
				{
					collision.GetComponent<PlayerController>().FlipGravity();
				}
			}
		}

		private void OnTriggerExit2D(Collider2D collision)
		{
			if (collision.CompareTag("Player"))
			{
				if (Color != collision.gameObject.GetComponent<SpriteRenderer>().color)
				{
					collision.GetComponent<PlayerController>().FlipGravity();
				}
			}
		}
	}
}
