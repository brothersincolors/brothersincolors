﻿using UnityEngine;

namespace Controllers.Traps
{
    public class SawRotatorController : MonoBehaviour
    {
        // Крутилка
        public float Angle = 10;

        private void FixedUpdate()
        {
            transform.RotateAround(transform.position, Vector3.forward, Angle);
        }
    }
}
