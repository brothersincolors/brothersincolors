﻿using Controllers.GameControllers;
using Controllers.Player;
using UnityEngine;

namespace Controllers.Traps
{
    public class ColorBlockController : MonoBehaviour
    {

        public Color Color;

        private PlayerController _player;
        private SpriteRenderer _attRenderer;

        // Выбор слоя в зависимости от цвета
        private void Start()
        {
            if (Color == MainGameplayController.Instance.Colors["Red"])
                gameObject.layer = LayerMask.NameToLayer("Red");
            else
                if (Color == MainGameplayController.Instance.Colors["Green"])
                gameObject.layer = LayerMask.NameToLayer("Green");
            else
                    if (Color == MainGameplayController.Instance.Colors["Blue"])
                gameObject.layer = LayerMask.NameToLayer("Blue");
            else
                        if (Color ==MainGameplayController.Instance.Colors["Yellow"])
                gameObject.layer = LayerMask.NameToLayer("Yellow");
            else
                            if (Color == MainGameplayController.Instance.Colors["Magenta"])
                gameObject.layer = LayerMask.NameToLayer("Magenta");
            else
                                if (Color == MainGameplayController.Instance.Colors["Cyan"])
                gameObject.layer = LayerMask.NameToLayer("Cyan");
            else
                                    if (Color == MainGameplayController.Instance.Colors["White"])
                gameObject.layer = LayerMask.NameToLayer("White");
            _attRenderer = GetComponent<SpriteRenderer>();
            _attRenderer.color = Color;
        }

        private void OnValidate()
        {
            GetComponent<SpriteRenderer>().color = Color;
        }
    }
}