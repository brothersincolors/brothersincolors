﻿using Controllers.Player;
using UnityEngine;

namespace Controllers.Traps
{
    public class FanController : MonoBehaviour
    {
        /* Подбрасывает в воздух */ 

        public Color Color;
        public float FanForce = 4f;

        SpriteRenderer _attRenderer;

        void Start()
        {
            if (!_attRenderer)
                _attRenderer = GetComponent<SpriteRenderer>();
            _attRenderer.color = Color;
        }

        void OnTriggerStay2D(Collider2D coll)
        {
            if (coll.CompareTag("Player"))
            {
                var player = coll.GetComponent<PlayerController>();
                Debug.Log(player.CurrentColor);
                if (Color == new Color32(128, 128, 128, 255) || player.CurrentColor == Color)
                {
                    player.JumpPower = FanForce;
                }
                else
                    player.JumpPower = player.JumpPowerCopy;
            }
                     
        }

		void OnTriggerExit2D(Collider2D coll)
		{
			if (coll.CompareTag("Player"))
			{
				var player = coll.GetComponent<PlayerController>();
				if (Color == new Color32(128, 128, 128, 255) || player.CurrentColor == Color)
				{
                    player.JumpPower = player.JumpPowerCopy;
				}
			}

		}

        private void OnValidate()
        {
            if (!_attRenderer)
                _attRenderer = GetComponent<SpriteRenderer>();
            else
                _attRenderer.color = Color;
        }

    }
}