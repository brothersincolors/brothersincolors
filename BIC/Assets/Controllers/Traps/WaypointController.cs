﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace Controllers.Traps
{

    public class WaypointController : MonoBehaviour
    {
        public bool Loop = false;                  //если активно, то первое положение и последнее должно быть одинаково
        public bool DynamicEnviron = false;        //окрестность меняется в зависимости от скорости и частоты кадров
        public float MagnitudeEnviron = 0.2f;
        public List<Motion> Motions;

        private float _delay = 0;
        private Rigidbody2D _attRigidbody;
        private InformationOfCurrentMotion _current;

        private void Start()
        {
            _current = new InformationOfCurrentMotion((Vector2)transform.position + Motions[0].VectorDistance(), Motions[0], 0);
            if (!gameObject.GetComponent<Rigidbody2D>()) gameObject.AddComponent<Rigidbody2D>();
            _attRigidbody = gameObject.GetComponent<Rigidbody2D>();
            _attRigidbody.gravityScale = 0;
            MagnitudeEnviron = DynamicEnviron ? 2 * Time.deltaTime * _current.Motion.Velocity : MagnitudeEnviron;
        }

        private void Update()
        {
            if (_delay > 0)
            {
                _delay -= Time.deltaTime;
                return;
            }
            _delay = 0;
            Mover();

        }

        private void Mover()
        {
            if (InEnviron(_current.Coords - (Vector2)transform.position, MagnitudeEnviron))
            {
                _attRigidbody.velocity = _current.Motion.VectorVelocity();
            }
            else
            {
                if (_current.Number == Motions.Count - 1)
                {
                    if (!Loop)
                    {
                        Motions.Reverse();
                        foreach (var motion in Motions)
                        {
                            motion.Direction *= -1;
                        }
                    }

                    _current.Number = -1;

                }

                _current.Number++;
                _current.Motion = Motions[_current.Number];
                _current.Coords = (Vector2)transform.position + Motions[_current.Number].VectorDistance();

                _delay = _current.Motion.Delay;
                _attRigidbody.velocity = Vector2.zero;

                MagnitudeEnviron = DynamicEnviron ? 2 * Time.deltaTime * _current.Motion.Velocity : MagnitudeEnviron;
            }

        }

        private bool InEnviron(Vector2 Delta, float MagnitudeEnviron) => Delta.magnitude >= MagnitudeEnviron;

    }

    public struct InformationOfCurrentMotion
    {
        public InformationOfCurrentMotion(Vector2 coords, Motion motion, int number)
        {
            this.Coords = coords;
            this.Motion = motion;
            this.Number = number;
        }

        public Vector2 Coords;
        public Motion Motion;
        public int Number;
    }

    [System.Serializable]
    public class Motion
    {
        public Motion(float velocity, float time, float delay)
        {
            this.Velocity = velocity;
            this.Time = time;
            this.Delay = delay;
        }

        public float Velocity;
        public float Time;
        public float Delay;
        public Vector2 Direction;

        public Vector2 VectorDistance() => Velocity * Time * Direction.normalized;
        public Vector2 VectorVelocity() => Velocity * Direction.normalized;
    }
}