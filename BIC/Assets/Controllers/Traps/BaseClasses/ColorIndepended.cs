﻿using Controllers.GameControllers;
using Controllers.Player;
using UnityEngine;

namespace Controllers.Traps.BaseClasses
{
    public class ColorIndepended : MonoBehaviour
    {
        /* Убивает всех */

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
                MainGameplayController.Instance.RestartFromCheckpoint(collision.GetComponent<PlayerController>());
        }
    }
}
