﻿using Controllers.GameControllers;
using Controllers.Player;
using UnityEngine;

namespace Controllers.Traps.BaseClasses
{
    public class ColorDepended : MonoBehaviour
    {
        /* Класс убивалка. */

        public Color Color;
        public bool FindComponentInChildren;        // Поиск спрайта в детях

        protected SpriteRenderer AttRenderer;

        protected virtual void Start()
        {
			AttRenderer = GetComponent<SpriteRenderer>();
			AttRenderer.color = Color;
        }


        // Убивает если цвета отличаются
        protected void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                if (Color != collision.gameObject.GetComponent<SpriteRenderer>().color)
                {
                    MainGameplayController.Instance.RestartFromCheckpoint(collision.GetComponent<PlayerController>());
                }
            }
        }

        // Меняет цвет в сцене что бы было видно какого цвета ловушка
        protected virtual void OnValidate()
        {
            if (FindComponentInChildren)
            {
                var tempRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
                if (tempRenderer) tempRenderer.color = Color;
            } else
            {
                var tempRenderer = GetComponent<SpriteRenderer>();
                if (tempRenderer) tempRenderer.color = Color;
            }
        }
    }
}
