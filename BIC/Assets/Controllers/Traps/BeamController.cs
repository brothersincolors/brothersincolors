﻿using Controllers.Traps.ActivationSystem;
using UnityEngine;

namespace Controllers.Traps
{
    public class BeamController : ActivationCompatibleObjectController
    {
        /* Луч. При активации меняет свой цвет */
        public Color ColorAfterActivate;

        protected override void Start()
        {
			AttRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
			AttRenderer.color = Color;
        }

        public override void Activate()
        {
            base.Activate();
            if (CurrentActiveActivatorsCount < ActivatorsCount) return;
            AttRenderer.color = ColorAfterActivate;
        }

        public override void Deactivate()
        {
            base.Deactivate();
            AttRenderer.color = Color;
        }
    }
}