﻿using System.Collections;
using Controllers.Player;
using Controllers.Traps.ActivationSystem;
using UnityEngine;

namespace Controllers.Traps
{
    public class LeverController : ActivatorController
    {
        /* Рычаг */
        public Color Color;
        public float Delay = 0.1f;

        SpriteRenderer _attRenderer;

        protected override void Start()
        {
            base.Start();
            _attRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
            _attRenderer.color = Color;
        }

        public void ChangeState(PlayerController player)
        {
            StartCoroutine(ChangeStateAfterDelay(player));
        }

        IEnumerator ChangeStateAfterDelay(PlayerController player)
        {
            yield return new WaitForSeconds(Delay);
            if (Color == new Color32(128, 128, 128, 255) || player.CurrentColor == Color)
            {
                ActivateSystem();
            }
        }

        protected override void ActivateSystem()
        {
            base.ActivateSystem();
			_attRenderer.flipX = !_attRenderer.flipX;
		}
    }
}