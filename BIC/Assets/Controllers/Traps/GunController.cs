﻿using Controllers.Traps.BaseClasses;
using Extensions.Classes;
using UnityEngine;

namespace Controllers.Traps
{
    [RequireComponent(typeof(Pooler))]
    public class GunController : MonoBehaviour
    {

        public GameObject BulletPrefab;
        public float RateTime;
        public Color BulletColor;
        public float Delay;

		private Vector3 _bulletDirection = Vector3.left;
		private float _currentTime;
        private Transform _muzzle;

        private Pooler _pooler;

        protected virtual void Start()        // Инициализируем пулер и костыль
        {
            _pooler = GetComponent<Pooler>();
            if (_pooler == null)
                _pooler = gameObject.AddComponent<Pooler>();
            _pooler.Initialize(BulletPrefab, 15);
            _muzzle = transform.GetChild(0);  
            _currentTime = Time.timeSinceLevelLoad +  Delay;
        }

        private void Update()
        {
            // Стрельба
            if (_currentTime < Time.timeSinceLevelLoad)
            {
                _currentTime = Time.timeSinceLevelLoad + RateTime;
                var tempBullet = _pooler.ActivateObject(_muzzle.position, Quaternion.identity);
                tempBullet.transform.localRotation = transform.localRotation;
                var tempComponent = tempBullet.GetComponent<BulletController>();
                tempComponent.Direction = _bulletDirection;
                tempComponent.Color = BulletColor;
            }
        }
    }
}
