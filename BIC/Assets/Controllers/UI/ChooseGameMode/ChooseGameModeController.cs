﻿using Events;
using UnityEngine;

namespace Controllers.UI.ChooseGameMode
{
	public class ChooseGameModeController : MonoBehaviour
	{

		public void SetGameModeToSolo() => GameStateMachine.CurrentGameMode = GameStateMachine.GameMode.Solo;

		public void SetGameModeToDuo() => GameStateMachine.CurrentGameMode = GameStateMachine.GameMode.Duo;

		public void SetGameModeToTripple() => GameStateMachine.CurrentGameMode = GameStateMachine.GameMode.Tripple;
	}
}
