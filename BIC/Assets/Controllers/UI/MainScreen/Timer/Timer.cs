﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{

    public int StartSecond = 0;
    public float BoostTime = 1f;

    private float PrevTime = Time.time;
    private int DeltaTime;
    private Text TimeUI;

    void Start()
    {
        DeltaTime = 0;
        TimeUI = gameObject.GetComponent<Text>();
    }

    void Update()
    {
        if (BoostTime * (Time.time - PrevTime) >= 1)
        {
            DeltaTime += 1;
  
            TimeUI.text= Format(DeltaTime / 3600, (DeltaTime / 60) % 60, DeltaTime % 60);
            PrevTime = Time.time;
        }
    }

    string Format(int h = 0, int m = 0, int s = 0)
    {
        string timeUI = "";
        if (h != 0)
        {
            timeUI += (h < 10 ? "0" : "") + h + ":";
        }

        timeUI += (m < 10 ? "0" : "") + m + ':';
        timeUI += (s < 10 ? "0" : "") + s;

        return timeUI;
    }
}
