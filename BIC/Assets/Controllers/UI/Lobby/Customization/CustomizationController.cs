﻿using Configs;
using Configs.Player;
using DataBases.SkinDataBase;
using InputControl;
using UnityEngine;
using Views.Lobby.Customization;

namespace Controllers.UI.Lobby.Customization
{
	public class CustomizationController : MonoBehaviour
	{
		public CustomizationView CustomizationView;
		public DefaultPlayerColor PlayerColor;
		public PlayerKeyboardControls KeyboardControls { get; private set; }
		public InputDevice InputDevice { get; private set; }
		public bool Activated;
		
		private SkinModel[] _playerSkins;
		private int _currentSkinIndex;

		private int CurrentSkinIndex
		{
			get { return _currentSkinIndex; }

			set
			{
				_currentSkinIndex = value;
				if (_currentSkinIndex >= _playerSkins.Length)
					_currentSkinIndex = 0;
				if (_currentSkinIndex < 0)
					_currentSkinIndex = _playerSkins.Length - 1;
			}
		}

		public SkinModel GetNextSkin()
		{
			CurrentSkinIndex++;
			Preferences.Prefs.Session.CurrentSkins[(int) PlayerColor] = _playerSkins[CurrentSkinIndex].Name;
			return _playerSkins[CurrentSkinIndex];
		}

		public SkinModel GetPreviousSkin()
		{
			CurrentSkinIndex--;
			Preferences.Prefs.Session.CurrentSkins[(int) PlayerColor] = _playerSkins[CurrentSkinIndex].Name;
			return _playerSkins[CurrentSkinIndex];
		}

		public void Activate(InputDevice inputDevice, PlayerKeyboardControls playerKeyboardControls)
		{
			Preferences.Prefs.PlayerInputDevices[(int) PlayerColor] = inputDevice;
			Preferences.Prefs.PlayerKeyboardControls[(int) PlayerColor] = playerKeyboardControls;
			InputDevice = inputDevice;
			KeyboardControls = playerKeyboardControls;
			Activated = true;
			CustomizationView.Activate();
		}

		public void Deactivate()
		{
			Preferences.Prefs.PlayerKeyboardControls[(int) PlayerColor] = null;
			Preferences.Prefs.PlayerInputDevices[(int) PlayerColor] = null;
			InputDevice?.FreeDevice();
			Activated = false;
			CustomizationView.Deactivate();
		}

		public void DeactivateWithoutClose() => Activated = false;

		public void ActivateUnclosed() => Activated = true;
		
		private void Start()
		{
			_playerSkins = SkinDataBase.Instance.GetPlayerSkins(PlayerColor);
		}

	}
}
