﻿using System.Linq;
using Configs.Player;
using Controllers.LevelManagement;
using Controllers.UI.Lobby.Customization;
using DataBases.KeycodesDataBase;
using InputControl;
using InputControl.DeviceClass;
using UnityEngine;

namespace Controllers.UI.Lobby
{
	public class SoloLobbyController : MonoBehaviour {

		public CustomizationController[] CustomizationControllers;

		private InputDevice _inputDevice;
		private PlayerKeyboardControls _playerKeyboardControls;

		public static SoloLobbyController Instance { get; private set; }

		private int _customizationControllerIndex = 0;
		private int _activatedToggles;

		
		public int ActivatedToggles
		{
			get { return _activatedToggles; }
			set
			{
				if (value - _activatedToggles > 0)
					ActivateNextCustomizationController();
				_activatedToggles = value;
				if (_activatedToggles == 3)
					LoadLevel();
			}
		}

		private async void LoadLevel()
		{
			Preferences.Prefs.SavePreferences();
			await SceneManagmentController.Instance.LoadSceneByName("scene_ui_splash_controls_solo", 2f);
		}

		private async void ActivateNextCustomizationController()
		{
			if (ActivatedToggles == 3) return;
			CustomizationControllers[_customizationControllerIndex].DeactivateWithoutClose();
			_customizationControllerIndex++;
			await new WaitForEndOfFrame();
			if (_customizationControllerIndex == 3) return;
			CustomizationControllers[_customizationControllerIndex].Activate(_inputDevice, _playerKeyboardControls);
		}

		private void DeactivateCurrentCustomizationController()
		{
			CustomizationControllers[_customizationControllerIndex].DeactivateWithoutClose();
			_customizationControllerIndex--;
			CustomizationControllers[_customizationControllerIndex].CustomizationView.ReadyToggle.Click();
		}

		private void Awake()
		{
			Instance = this;
		}
		
		private void Start() => _playerKeyboardControls = new PlayerKeyboardControls(1);

		private void Update()
		{
			if (_customizationControllerIndex != 0)
			{
				if (_inputDevice != null && _inputDevice.DeviceIsConnected &&
				    _inputDevice.GetKeyDown(GamepadKeycode.Action2))
					DeactivateCurrentCustomizationController();
				if (Input.GetKeyDown(KeyCode.Escape))
					DeactivateCurrentCustomizationController();
			}
			
			if (_customizationControllerIndex != 0) return;
			
			Device device = null;
			if (InputManager.GlobalJoystick.GetKeyDown(GamepadKeycode.Start, ref device))
			{
				if (!device.isUsed)
				{
					_inputDevice = new InputDevice(device);
					CustomizationControllers[0].Activate(_inputDevice, null);
				}
				else
				{
					CustomizationControllers[0].Deactivate();
				}
			}
			else
			{
				if (Input.GetKeyDown(_playerKeyboardControls.Interact))
				{
					if (!CustomizationControllers[0].Activated)
						CustomizationControllers[0].Activate(null, _playerKeyboardControls);
					else
						CustomizationControllers[0].Deactivate();
				}
			}
				
		}
	}
}
