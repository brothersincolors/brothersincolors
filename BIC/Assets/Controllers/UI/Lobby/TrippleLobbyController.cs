﻿using System.Linq;
using Configs.Player;
using Controllers.LevelManagement;
using Controllers.UI.Lobby.Customization;
using DataBases.KeycodesDataBase;
using InputControl;
using InputControl.DeviceClass;
using UnityEngine;

namespace Controllers.UI.Lobby
{
	public class TrippleLobbyController : MonoBehaviour
	{
		public CustomizationController[] CustomizationControllers;
		
		private int _activatedToggles;
		private readonly PlayerKeyboardControls[] _playerKeyboardControls = new PlayerKeyboardControls[3];

		public static TrippleLobbyController Instance { get; private set; }

		public int ActivatedToggles
		{
			get { return _activatedToggles; }
			set
			{
				_activatedToggles = value;
				if (_activatedToggles == 3)
					LoadLevel();
			}
		}
		
		private async void LoadLevel()
		{
			Preferences.Prefs.SavePreferences();
			await SceneManagmentController.Instance.LoadSceneByName("scene_ui_splash_controls_tripple", 2f);
		}

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			for (var i = 0; i < 3; i++)
				_playerKeyboardControls[i] = new PlayerKeyboardControls(i);
		}

		private void Update()
		{
			Device device = null;
			if (InputManager.GlobalJoystick.GetKeyDown(GamepadKeycode.Start, ref device))
			{
				if (!device.isUsed)
				{
					var inputDevice = new InputDevice(device);
					CustomizationControllers.First(controller => !controller.Activated).Activate(inputDevice, null);
				}
				else
				{
					CustomizationControllers.First(controller => controller.InputDevice.CompareDevices(device)).Deactivate();
				}
			}
			else
			{
				for (var i = 0; i < 3; i++)
				{
					if (Input.GetKeyDown(_playerKeyboardControls[i].Interact))
					{
						if (!CustomizationControllers[i].Activated)
							CustomizationControllers[i].Activate(null, _playerKeyboardControls[i]);
						else
							CustomizationControllers[i].Deactivate();
					}
				}
			}
		}
	}
}
