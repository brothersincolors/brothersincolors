﻿using System.Net.Mime;
using System.Runtime.Serialization.Formatters;
using Controllers.LevelManagement;
using UnityEngine;
using UnityEngine.UI;

namespace Controllers.UI.SplashScreen
{
	public class SplashScreenController : MonoBehaviour
	{
		public string SceneToLoad;
		public float PrepareTime = 0;
		public bool NeedAnyKeyToStart = true;
		
		private bool _loadingStarted;
		
		private void Update()
		{
			if (NeedAnyKeyToStart)
			{
				if (Input.anyKeyDown)
					LoadLevel();
			}
			else LoadLevel();

		}

		private async void LoadLevel()
		{
			if (_loadingStarted) return;
			_loadingStarted = true;
			await SceneManagmentController.Instance.LoadSceneByName(SceneToLoad, PrepareTime);
		}
	}
}
