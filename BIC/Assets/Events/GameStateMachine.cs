﻿using UnityEngine;

namespace Events
{
	public static class GameStateMachine {

		public enum GameState { Running, Paused, Loading, }
		public enum PlayersState { Alive, Dead }
		public enum GameMode { Solo, Duo, Tripple }

		public static GameState CurrentGameState;
		public static PlayersState CurrentPlayersState;
		public static GameMode CurrentGameMode = GameMode.Tripple;
	}
}
