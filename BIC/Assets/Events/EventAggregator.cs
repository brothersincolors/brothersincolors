﻿using System;

namespace Events
{
    public static class EventAggregator
    {

        // Player death
        
        private static event Action OnPlayerDeath;

        public static void SubscribeToPlayerDeath(Action callback) => OnPlayerDeath += callback;
        public static void UnsubscribeFromPlayerDeath(Action callback) => OnPlayerDeath -= callback;

        public static void PlayerDeath() => OnPlayerDeath?.Invoke();
        
        // Level loaded

        private static event Action OnLevelLoaded;

        public static void SubscribeOnLevelLoading(Action callback) => OnLevelLoaded += callback;
        public static void UnsubscribeFromLevelLoading(Action callback) => OnLevelLoaded -= callback;

        public static void LevelLoaded() => OnLevelLoaded?.Invoke();
        
        // Players spawned

        private static event Action OnPlayersSpawned;

        public static void SubscribeOnPlayersSpawn(Action callback) => OnPlayersSpawned += callback;
        public static void UnsubscribeFromPlayersSpawn(Action callback) => OnPlayersSpawned -= callback;

        public static void PlayersSpawned() => OnPlayersSpawned?.Invoke();

    }
}
